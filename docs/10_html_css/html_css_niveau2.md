---
title: "HTML CSS niveau 2"
papersize: a4
geometry: margin=2cm
fontsize: 12pt
lang: fr
---


Récupérer préalablement l'archive [html_css_avance](./html_css_avance.zip) et décompressez-la dans votre répertoire de travail.  

Votre travail se déroule en deux temps :

-	En classe, vous allez réaliser la présentation d’un film
-	A la maison, vous devez préparer une page web présentant trois films.


# Travail en classe (2H)

-	Vous choisissez votre film
-	La structure HTML est imposée par le document HTML fourni dans le dossier `presentation_film`
-	Le document CSS est déjà lié au document HTML.
-	Le document CSS ne comporte que les instructions sur la balise body. 


__Vous devez le compléter pour avoir un document web proche visuellement de la photo fournie : `presentation_un_film.png`__

__1)__ Analysez précisément la structure des fichiers proposés  
__2)__ Les fichiers sont placés dans des dossiers différents, vous devrez en tenir compte en utilisant des chemins reatifs(_voir AIDES_)  
__3)__ Vous devez réaliser un affichage en utilisant des __flex-box__ principalement sur les éléments d'id menu et texte. (_utilisez les sites fournis en AIDES pour comprendre cet affichage_)


# Projet (à la maison)

-	Vous choisissez trois films que vous présentez sur une page web.
-	La structure HTML est imposée par le document HTML fourni dans le dossier `projet_site_films`. Respectez-la.
-	Le document CSS est déjà lié au document HTML.
-	Vous devez vous inspirer de la présentation du document correction présenté en classe 
-	La présentation des trois films doit tenir sur une page web sans scroll.
-   Des transitions doivent être mises en place pour passer d'un film à l'autre.
-   Vous devez utiliser des flex-box.


# AIDES : 

__1) Disposition des éléments en flex__

Voir les sites suivants pour comprendre l’utilisation des flex-box :   

[https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox)  

[https://www.alsacreations.com/tuto/lire/1493-css3-flexbox-layout-module.html](https://www.alsacreations.com/tuto/lire/1493-css3-flexbox-layout-module.html)  

[https://www.pccindow.com/fr/flexbox-css/](https://www.pccindow.com/fr/flexbox-css/)


__2) Chemins relatifs__

[https://www.alsacreations.com/astuce/lire/78-Quelle-est-la-difference-entre-les-chemins-relatifs-et-absolus-.html](https://www.alsacreations.com/astuce/lire/78-Quelle-est-la-difference-entre-les-chemins-relatifs-et-absolus-.html)  

__3) Propriétés de base en CSS (_liste non exhaustive_)__

|Propriétés|Valeurs|  
|---|:---:|  
|**width** : Largeur d’un élément|__px__ (pixel) ou __vw__(pourcentage de la largeur de la fenêtre)|  
|**height**: Hauteur d’un élément(Valeur défini que pour certaines balises)|  __px__ (pixel) ou __vh__(pourcentage de la hauteur de la fenêtre)|  
|**margin**: marge extérieure de l’élément|__px__ (pixel) __vh__ ou __vw__|  
|**padding** : marge intérieure|__px__ (pixel) __vh__ ou __vw__|  
|**background-color** : couleur de fond|: __nom de la couleur__ (_red_) __valeur décimale__ (_rgb(255,0,0)_) ou __valeur hexadécimale__ (_#FF0000_) |  
|**color**: couleur de la police| comme pour color|  
|**opacity**: opacité d’un élément|Valeur comprise entre _0_ et _1_|  
|**font-size** : Taille de la police|__em__ __px__ (__vh__ et __vw__ sont possibles)|  



__4) Réaliser des transitions__ 

https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Transitions/Utiliser_transitions_CSS  


__5) Site généraliste en CSS3__

  
http://www.css3create.com/  