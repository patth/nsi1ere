---
title: "Prérequis HTML et CSS"
papersize: a4
geometry: margin=1.5cm
fontsize: 10pt
lang: fr
---

# Le langage HTML

Récupérer préalablement l'archive [html_css_debutant](./html_css_debutant.zip) et décompressez-la dans votre répertoire de travail.  

## Qu’est-ce que le langage HTML ?

__HTML__ signifie _HyperText Markup Langage_ ce qui se traduit par langage de balisage hypertexte. Le langage HTML est  un langage de description qui structure une page web.

Un fichier de format HTML qui n'est qu'un simple fichier texte va être __interprété__ par un __navigateur web__ qui rendra les titres sous une certaine forme, affichera en gras ou en italique les caractères devant l'être, permettra de suivre des liens sur d'autres pages web ou d'autres sites web, etc..

On distinguera deux types de logiciels :

|les `navigateurs`|les `éditeurs`|    
|:---:|:---:|     
|_Chrome, Firefox, Edge, Safari ..._| _Notepad++, Visual Studio Code, Atom ..._|  
|ils interprétent le code html afin de réaliser une affichage à destination de l'utilisateur | ils permettent de modifier le code |  

__📝Application__ : 

> * Ouvrez le fichier `exemples_cours/1_explication_head.html` avec le navigateur de votre choix.
> * Visualisez le code source `CTRL + U` : constatez que vous ne pouvez pas le modifier

Les éléments HTML rencontrés sont délimités soit par:  

* un couple de balise (ouvrantes / fermante): `<balise> </balise>`
* soit par une seule balise (autofermante) :  `</balise>`

## Structure générale d’une page HTML

Un fichier codé en HTML sera interprété par un navigateur. Il faut donc dans le document HTML préciser plusieurs éléments afin que l’interprétation soit réussie.

* `<!DOCTYPE html>` : cette balise précise au navigateur que le type du document est HTML.
* L’élément racine html délimité par  les balises `<html> </html>`   qui correspondent au début et la fin du document.
* Puis, les éléments `head` et `body` (tous les deux délimités par un couple de balises) qu’on retrouve à l’intérieur de l’élément html.
* Remarquez qu'aucun texte n'est présent entre les deux balises `body` (cela correspond au corps du document) : c'est pour cela que rien n'apparait sur la page dans votre navigateur.

## Présentation du « head »

* On y on fixe l’encodage des caractères.
* On y réalise les liens vers les fichiers externes.

Afin de s'approprier la syntaxe du code html, nous allons modifier quelques parties du code présenté à l'aide d'un éditeur. Le choix se portera sur `Notepad++` téléchargeable [ici](https://notepad-plus-plus.org/). D'autres éditeurs sont bien évidemment utilisables comme ceux cités en introduction.

__📝Application__ : 

> * Ouvrez le fichier `exemples_cours/1_explication_head.html` avec Notepad++ (soit depuis le logiciel soit à partir de l'explorateur clic droit : `Edit with Notepad++`)
> * Observez les commentaires délimités par les caractères `<!--` et  `-->`, comme en Python ils ne sont pas interprétés.
  
![head](./fig/head.png)

### Encodage des caractères ![](./fig/charset.png)


* C’est une balise de type `meta` qui fournit des métadonnées au navigateur.
* elle est auto fermante car elle termine par `/>`.
* Le nom de la balise (meta) est suivi d’un __attribut__  : `charset` associé à une __valeur__ : `utf-8`.  
* Cette ligne d’instruction s’occupe de l’encodage des caractères. Elle s’occupe donc de préciser au navigateur avec quel jeu de caractères il devra réaliser l’affichage.

__📝Application__ : 

> * A partir de l'éditeur : insérer un texte de votre choix avec des caractéres accentués dans la partie `body`. Enregistrez.
> * A partir du navigateur : actualisez la page `F5` et observez ce texte
> * A partir de l'éditeur : supprimez la ligne `<meta charset="utf-8"/>`. Enregistrer. puis observez dans le navigateur. La présence de cette ligne est cruciale pour l'affichage des caractères accentués notamment.


### meta-informations  ![](./fig/meta_name.jpg)

* La deuxième meta information permet de nommer l’auteur de la page HTML.
*  C’est une métadonnée donc elle n’est pas visible sur la page lors de l’affichage. On peut retrouver cette indication dans les propriétés du document.

__📝Application__ : 

> * A partir du navigateur, rélalisez un clic droit sur la page, choisissez `Informations sur la page` et observez la meta information précédente.
> * A partir de l'éditeur, changer la valeur de l'attribut `content` en remplaçant `NSI` par votre nom. Enregistrez, puis dans le navigateur actualisez la page. Réaffichez les informations sur la page pour visualiser la modification.

### Titre du document ![](./fig/title.jpg)

Cette ligne permet de mettre un nom à l’onglet.

__📝Application__ : 

> Modifiez le texte présent entre les deux balises `title`. Enregistrez et observez le changement du texte affiché dans l'onglet du navigateur correspondant à la cette page.    

_Remarques_ : Comme en Python, il est important d’indenter le code pour en faciliter sa lecture. Cependant, contrairement à Python elle n’est pas oligatoire.
 

## Présentation du body

C’est dans cette zone qu’on réalise ce qui va être affiché à l’écran. On peut mettre en forme du texte en utilisant des balises adéquates. Voici quelques balises très courantes permettant de structure le texte.

__📝Application__ : 

> * visualiser au fur et mesure les fichiers présents dans le répertoire `exemples_cours` correspondants aux différentes balises ci-après à la fois dans l'éditeur et dans le navigateur.
> * Pour chacun de ces éléments, tentez de modifier le contenu pour observer le comportement en résultant.


### Les titres

|__Titres__|Affichage dans le navigateur|
|:-----:|:------:|
|Les éléments `h1` à `h6` permettent de réaliser 6 niveaux de titres |![](./fig/titres.png)|

```html
<h1>Titre1</h1> 
    <h2>Titre2</h2>
        <h3>Titre3</h3>
```

### Les paragraphes

|__Paragraphes__|Affichage dans le navigateur|    
|:-----:|:------:|          
|L’élément `p` délimite le paragraphe. Un saut de ligne  est placé avant et après. |![](./fig/p.png)|  

```html
<p>La balise p permet de donner une structure de paragraphe à l’ensemble d’un texte.</p>
```

### Les sauts de ligne

|__Saut de ligne__|Affichage dans le navigateur|
|:---:|:----:|
|La balise `<br/>`|![](./fig/br.png)|  

```html
<p>L'élément br permet de réaliser un saut de ligne <br/> sans elle aucun
 retour 
 chariot 
 n'est interpré.</p>
```

### Les listes

|__Listes non ordonnées__|Affichage dans le navigateur|  
|:---:|:----:|    
|Permettent de lister du texte sans le numéroter. l'élément `ul` délimite la liste. Les éléments `li` emboités à l'intérieur représentent chacun un élément de la liste.|![](./fig/listes.png)|  

```html
<ul>
    <li> Première étape.</li>
    <li> Deuxième étape.</li>
    <li> Troisième étape.</li>
</ul>
```

__📝Exercice n°1__ : 

> Utiliser le fichier `exercice1.html` et mettez en forme le texte en utilisant uniquement les balises précédentes. Une photo `correction_exercice_1.png` vous donne un visuel de ce que vous devez réaliser.


###  Insérer une image
L'élément `img` est une balise auto fermante. L’attribut `src` permet de donner le chemin relatif vers l’image.

```html
<img src = "photo.jpg"/>
```

* Ne pas oublier l’extension.
* Le chemin relatif se fait ici par rapport au fichier HTML.

### Créer des liens hypertextes

L'élément `a` est utilisé pour créer des liens vers d’autres pages web ou pour se rendre rapidement à d’autres endroits de la page :
  
```html
<a href = "lien vers une page web">lien</a>
```

Si le lien est actif, il apparait en bleu et il est souligné.

### Les balises div : balises génériques

L’élément `div` est utilisé pour partitionner une page web. 
C’est un élément générique sans signification particulière
	
```html
<div>Première page HTML</div>
```


__📝Exercice n°2__ : 

> Vous allez structurer une page web en utilisant le matériel proposé et en respectant le visuel fourni.

# Le langage CSS

## Qu’est-ce que le CSS ?

Les __CSS__, _Cascading Style Sheets_ (feuilles de styles en cascade), servent à mettre en forme des documents web. Par l'intermédiaire de propriétés d'apparence (couleurs, polices, etc…) et de placement (largeur, hauteur, etc...), notamment. Les feuilles de styles ont d'ailleurs pour objectif principal de dissocier le contenu de la page de son apparence visuelle

## Comment lier les documents HTML et CSS ?  

Il faut ajouter un lien dans le head du fichier HTML.

```html
<link rel="stylesheet" href="style.css"/>
```

Dans cette balise, la valeur associée à l’attribut __href__ permet d’indiquer le chemin vers le fichier externe codé en CSS. On n’oubliera pas de mettre  l’extension __.css__ après le nom du fichier. Le chemin est un chemin relatif depuis le fichier HTML.

__📝Application__ : 

> * Ouvrez le fichier `exercice_3/index.html` d'abord avec votre navigateur puis avec votre éditeur.
> * Observez la balise link présente dans le `head` à la ligne 8 :  elle est pour l'instant commentée. Décommentez-la. Enregistrez puis actualiser la page sur votre navigateur.


## Comment appliquer un style à un élément présent dans le code HTML ?

__📝Application__ : 

> * Ouvrez le fichier `exercice_3/style.css` avec votre éditeur.
> * Notez la présence de commentaires vous aidant à comprendre les différentes propriétés modifiées. Ces commentaires en CSS sont délimités par `/*` et `*/` .


Pour appliquer un style à une ou plusieurs éléments, on sélectionne les éléments de différentes manières.

### Sélection des éléments par leur nom

```css
p {
    color:red;
}
```
Ici le séléecteur est `p` : tous les paragraphes sont sélectionnés.
 On ouvre une accolade `{` , on donne la propriété CSS (ici `color` la couleur du texte) et on lui affecte une valeur (ici `red`).
 On peut définir plusieurs couples propriétés / valeurs entre les accolades. Il faut toujours terminer une ligne par un point-virgule `;` et refermer l’accolade `}` à la fin du bloc de propriétés pour un sélecteur donné.

### Sélection des éléments par leur identificant (id)

On peut spécifier un élément html en lui ajoutant un attribut `id`

__📝Application__ : 

> * Cherchez dans le code du fichier `index.html` l'élément d'id `texte`
> ```html
> <div id = "texte"></div>
> ```
> * Constatez qu'aucun autre élément ne posséde la même valeur pour l'attribut `id` : __un identifiant doit être unique__

En CSS, pour sélectionner cet élément on utilise le caractère `#` suivi de la valeur de l’attribut id

```css
#texte {
    width: 30vw;
}
```

### Sélection des éléments par leur classe.  

__📝Application__ : 

> * Cherchez dans le code du fichier `index.html` les éléments de classe `pourcentage`
> ```html
> <strong class="pourcentage">70,8 %</strong>  
> <strong class="pourcentage">96 %<strong>  
> ```
> * Constatez que __plusieurs éléments peuvent appartenir à la même classe__: c'est la différence principale avec l'identifiant.


En CSS, pour sélectionner ces éléments on utilise le caractère `.` suivi de la valeur de l’attribut `class`

```css
.pourcentage {
    color :pink;
}
```

## Quelques propriétés courantes en CSS

__📝Application__ : 

> Modifiez une à une les valeurs des propriétés présentes dans la feuille de style pour bien comprendre leur utilisation.  

|Propriétés|Valeurs|  
|---|:---:|  
|**width** : Largeur d’un élément|__px__ (pixel) ou __vw__(pourcentage de la largeur de la fenêtre)|  
|**height**: Hauteur d’un élément(Valeur défini que pour certaines balises)|  __px__ (pixel) ou __vh__(pourcentage de la hauteur de la fenêtre)|  
|**margin**: marge extérieure de l’élément|__px__ (pixel) __vh__ ou __vw__|  
|**padding** : marge intérieure|__px__ (pixel) __vh__ ou __vw__|  
|**background-color** : couleur de fond|: __nom de la couleur__ (_red_) __valeur décimale__ (_rgb(255,0,0)_) ou __valeur hexadécimale__ (_#FF0000_) |  
|**color**: couleur de la police| comme pour color|  
|**opacity**: opacité d’un élément|Valeur comprise entre _0_ et _1_|  
|**font-size** : Taille de la police|__em__ __px__ (__vh__ et __vw__ sont possibles)|  

**Concernant les couleurs** :

* Couleur exprimée en hexadécimale :  Les deux premiers symboles après # représente la valeur du rouge, le deux suivants la valeur du vert et les deux derniers celle du bleu.
  
* Couleur exprimée en décimale avec transparence: _rgba(255, 255, 255, 0.5)_
le premier paramètre correspond à la valeur du rouge (0 à 255),  le deuxième : la valeur du vert, le troisième la valeur du bleu et le dernier paramètre la transparence.

__Concernant la disposition des éléments en flex__

*Flex-box* : Voir les sites suivants pour comprendre l’utilisation des flex-box :

[https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox)  

[https://www.alsacreations.com/tuto/lire/1493-css3-flexbox-layout-module.html](https://www.alsacreations.com/tuto/lire/1493-css3-flexbox-layout-module.html)  

[https://www.pccindow.com/fr/flexbox-css/](https://www.pccindow.com/fr/flexbox-css/)

# Projet 

* Vous choisissez un pays que vous présentez sur une page web.
* La structure HTML est imposée par le document HTML fourni (Vous devez le compléter)
* Le document CSS est déjà lié au document HTML.
* Vous devez vous inspirer du document `ocean.html` pour créer vos pages HTML et CSS mais votre version doit être différente.
* Le document HTML comportera obligatoirement en plus des balises présentes un lien hypertexte pointant vers la source de vos textes

_Remarques :_

* L'évaluation portera principalement sur la diversité des balises en HTML et la diversité des propriétés et des sélecteurs en CSS.
* Le contenu peut être récupéré sur une encyclopédie en ligne : ce n'est pas l'objet de la notation.
