---
title: "Chapitre 11  : Spécification et tests des programmes"
subtitle: "TP : Documenter et tester "
papersize: a4
geometry: margin=1.5cm
fontsize: 10pt
lang: fr
---

# Exercice 1 : Documentation au format resT &#x1F3C6;

__1.__ Soit la fonction suivante renvoyant la somme de dés 6 :

```python
def total_des(de1, de2):
    return de1 + de2
```

Ecrire une docstring complète vous rajouterez une contrainte d'utilisation

__2.__ Soit la procédure suivante affichant le nom, le prénom et la taille en mètres d'une personne :

```python
def affichage_infos(nom, prenom, taille):
    print("Mes informations personnelles :", nom, prenom, str(taille)+"m")
```

Ecrire une docstring complète


# Exercice 2 : Préconditions et postconditions  &#x1F3C6;

__1.__ Soit la fonction racine carrée appliquée sur un entier : trouver une précondition et deux postconditions (_plus difficile_) sur son utilisation. On ne s'interessera pas au type de données.

__2.__ Soit la fonction suivante renvoyant le solde d'un compte bancaire qu'on souhaite toujours crediteur (ou à zéro)

```python
def nouveau_solde(retrait, solde):
    solde = solde - retrait
    return solde
```
__a-__ Trouver une précondition et une postcondition  
__b-__ Ecrire la docstring correspondante  en ajoutant des contraintes d'utilisations


# Exercice 3 : Assertions  &#x1F3C6;

__1.__ Soit la fonction suivante `mention` renvoyant la mention obtenue au BAC pour une moyenne passée en paramètre:

```python
def mention(moyenne):
    if moyenne < 12:
        return None
    elif moyenne < 14:
        return 'AB'
    elif moyenne <16:
        return 'B'
    else :
        return 'TB'
```

__a-__ Ecrivez une assertion vérifiant le type du paramètre `moyenne` puis rédigez la docstring correspondante
__b-__ Ecrivez deux assertions permettant d'éviter des moyennes incohérentes.

_Remarque_ : lorsque plusieurs types de données sont possibles pour un paramètre on peut utiliser la syntaxe suivante :

```python
"""
:param a: description du paramètre a
:type a: bool or int
"""
```

Ici par exemple le paramètre `a` peut être un booléen ou un entier.

__2.__ Soit la fonction suivante renvoyant la valeur maximale présente dans un tableau d'entiers passé en paramètre

```python
def max_tableau(tab):
    max_value = tab[0]
    for nombre in tab :
        if nombre > max_value:
            max_value = nombre
    return max_value
```

__a-__ Quelle est la précondition sur le tableau passé en paramètre? (Pensez au cas limite)  
__b-__ Ecrivez l'assertion correspondante   
__c-__ Donnez une deuxième version `max_tableau_bis` de cette fonction sans utiliser d'assertion mais en utilisant la valeur `None`  comme valeur de retour.  


# Exercice 4 : Doctests  &#x1F3C6;
   
__1. Fonction puissance__

__a-__ Donner une chaîne de documentation (pour l'instant sans example) pour la fonction suivante qui calcule x à la puissance n pour deux entiers x et n.

```python
def puissance(x, n):
    return x ** n
```

__b-__ Etablir un jeu de quelques tests et charger le module doctest  au lancement du script comme vu en cours à partir du shell le module doctest comme en cours.

__c-__ Modifiez un des tests afin qu'il échoue. Observer le résultat.


__2. Fonction `max_tableau`__

__a-__ Reprendre la fonction `max_tableau` de l'exercice précédent et établissez une docstring compléte avec un jeu de tests que vous appliquerez.

__b-__ Les doctests peuvent aussi s'appliquer aux exceptions levées lors d'une erreur comme `AssertionError`.  
Voici un exemple d'utilisation dans la docstring: 

```python
"""
>>> max_tableau([])
Traceback (most recent call last):
    ...
AssertionError: le tableau ne peut être vide

"""
```
__Essayez d'intégrer cet exemple à vos doctests en adaptant le message d'erreur au votre__


# Exercice 5 Bon jeu de tests &#x1F3C6; &#x1F3C6; &#x1F3C6;
_Extrait de Numérique et Sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_

On prétend que le prédicat `appartient` défini juste après teste l'appartenance de la valeur `v` au tableau `t`.

```python
def appartient(v,t):
    i = 0
    while i < len(t)- 1 and t[i] !=v:
        i = i + 1
    return i < len(t)        
```

Par exemple : 

```python
>>> appartient(4, [1,3,4,8])

 True
```
  
__1.__ Etablissez un jeu de quelques tests (dans certains cas `v `est présent dans le tableau, dans d'autres non). Puis utilisez les avec le module doctest.
Vous devez constater des erreurs car la fonction ne réalise en fait pas ce qu'on attend d'elle.   
__2.__ Expliquez  ce que fait en réalité la fonction `appartient`  
