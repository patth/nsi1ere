---
title: "Chapitre 12 : Les tris "
subtitle : "Activité : tris sans ordinateur - version PROF"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


# Etape n°1 : Trier les boites

## Matériel initial à disposition (par groupe):

* 10 boites à vis de masse toutes différentes


## Consigne initiale : trier les boites (sans plus de précision)

* deux notions doivent émerger : comparateur et sens du tri  
   
* ne surtout pas donner de comparateur dans la consigne !!!  
  

# Etape n°2 : Décrire les tris

## Matériel supplémentaire mis à disposition (par groupe):

* Un comparateur (planche en bois + pivot)
* Une feuille A3

## Consignes supplémentaire

* formaliser ensuite par écrit et en français la méthode trouvée : les indications doivent être suffisamment claires pour que la méthode soit reproductible par un autre groupe sans explications supplémentaires.
Elle doit préciser les objets sur lesquelles elles opèrent et ne pas être une implémentation en python.

    
* Faire vérifier la méthode par le professeur
  
* Finaliser votre travail au propre sur la feuille A3 fournie
   

# Etape n°3 : Test de la méthode de tri
   
* Donner votre résumé à un autre groupe : vérifier que la méthode est clairement décrite en la déroulant en suivant étape par étape ce qu'ils ont écrit.(Essayer de donner des tris insertion àceux qui onttrouvé le tri sélection et inversement)


# Etape n°4 : Restitution

* Présenter la méthode à la classe (choisir 2 groupes représentant les 2 tris et demander qui reconnait sa méthode)
* Pour faciliter la restitution on peut util iser : http://www.nileb.net/nsi/algo01/balance.html (script de Nicolas Belin Faidherbe)


# Bilan sur les tris rencontrés

Tous les algorithmes de tri décrits ici prennent en :
- Entrée : _N_ boîtes 
- Sortie : Les mêmes boîtes alignées de la plus légère à la plus grande.

## Le tri par sélection

### Version 1 :
```
Je débute avec un alignement vide de boîtes triées
Tant qu'il y a des boîtes non triées :
   Je cherche la boite la plus légère parmi les boîtes non triées
   Je la place à la suite des boîtes déjà triées.
fin Tant que
```

Il est nécessaire de définir une méthode pour déterminer la plus légère des boîtes
parmi les boîtes non triées

```
Entrée : Des boîtes
Sortie : La boite la plus légère 
Effet de Bord : Enlève une boite 

Je prends une boîte 
Pour chacune des autres :
    Si cette boite est plus légère que la boite dans ma main,
    Alors je conserve la plus légère des deux.
	Fin Si
	Je mets l'autre de côté.
Fin Pour
```

### Version 2:

On considère que les boîtes sont alignées et on les repère par leur position de 
la gauche vers la droite.

```
Pour i allant de la première position à la dernière
   trouver la boite la plus légère parmi les boîtes aux positions i à N
   échanger la boite à la position i avec cette boite
fin pour
```

Il est nécessaire de définir une méthode pour déterminer la plus petite des boîtes
parmi les positions i à N 

```
Entrées : les boîtes, une position i (de départ)
Sortie : la position de la boîte la plus légère

Je repère la première boite par sa position
pour la deuxième boîte jusqu'à la dernière
  Si la boîte courante est plus légère que la boîte repérée,
  Alors la boîte courante est repérée
fin pour
renvoie la position de la boîte repérée
```


## Le tri par insertion


### Version 1
```
    Je débute avec un alignement vide de boîtes triées
    Tant qu'il y a des boîtes non triées :
       Je choisis une boite 
	   Je l'insère dans l'alignement de telle sorte qu'il reste trié
    fin Tant 
```

Il est important ici de présenter un algorithme d'insertion dans un alignement trié.

```
Entree : Un alignement de boîtes trié, une boîte b
Sortie : rien
Effet de bord: l'alignement reste trié

Prends la boîte la plus à droite (la plus lourde)
Tant que cette boite est plus lourde que b
     passe à la boite suivante
insère b à la droite de la boite courante
```

### Version 2
```
Pour i allant de la deuxième position à la dernière ($N$):
   insère la boite i dans les boîtes aux positions 1 à i-1
Fin Pour
```

```
Entree : une boite b à la position i

j = i-1
tant que la boite à la position j est plus légère que la boite b
   déplace la boite j vers la droite
fin tant que
insere la boite b dans l'espace libre
```


## Le tri rapide (si trouvé)


```
Entrée : un ensemble l de boîtes
Effet : les boîtes sont triées

S'il y a au moins deux boîtes dans l,
Alors 
  Choisis une boite b dans l
  Pour toutes les autres boîtes dans l
    	si la boîte est plus légère que b, range-la à gauche de b.
	    si la boîte est plus lourde que b, range-la à droite de b.
	    demande à un ami d'appliquer la même méthode pour les boîtes de droite, 
	    demande à un ami d'appliquer la même méthode pour les boîtes de gauche.
  fin Pour
Fin Si

```





