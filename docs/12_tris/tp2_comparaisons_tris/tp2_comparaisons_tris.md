---
title: "Chapitre 12 : les tris"
subtitle: "TP : Etude de différents algorithmes de tris"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice n° 1 : Tri par insertion et tableau décroissant &#x1F3C6;

_Extrait de Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_

Sans programmer, expliquer simplement ce qui se passe lorsque le tri par insertion est appliqué à un tableau qui se présente en ordre décroissant.


# Exercice n° 2 : Tableau trié ? &#x1F3C6;&#x1F3C6;

_Extrait de Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_

Ecrire un predicat `est_trie(t)` permettant de déterminer si le tableau de nombres passé en paramètre est trié par ordre croissant.


# Exercice n° 3 : QCM &#x1F3C6;

__1)__ Soit T le temps nécessaire pour trier, à l'aide de l'algorithme du tri par insertion, une liste de 1000 nombres entiers. Quel est l'ordre de grandeur du temps nécessaire, avec le même algorithme, pour trier une liste de 10 000 entiers, c'est-à-dire une liste dix fois plus grande ?

* [ ] à peu près le même temps T 
* [ ] environ 10 x T  
* [ ] environ 100 x T
* [ ] environ T


__2)__ Lors de l'exécution du code suivant, combien de fois l'opération `a = 2*a` sera-t-elle effectuée ?

```python
a = 1
cpt = 1
while cpt < 8:
    a = 2 * a
    cpt = cpt + 1
```

* [ ] 0
* [ ] 1
* [ ] 7
* [ ] 8

__3)__ La fonction suivante doit déterminer la valeur maximale d'un tableau de nombres passé en argument. Avec
quelles expressions faut-il remplacer les pointillés du script suivant pour que la fonction soit correcte ?

```python
def maximum(T):
    maxi = T[0]
    n = len(T)
    for i in range(1, .....):
        if T[i] > maxi:
            maxi = ......
    return maxi
```

* [ ] n puis T[i]
* [ ] n puis T[i-1]
* [ ] n-1 puis T[i]
* [ ] n-1 puis T[i-1]

__4)__ Quel est l’ordre de grandeur du coût du tri par insertion (dans le pire des cas) ?

* [ ] l'ordre de grandeur du coût dépend de l'ordinateur utilisé
* [ ] linéaire en la taille du tableau à trier
* [ ] quadratique en la taille du tableau à trier
* [ ] indépendant de la taille du tableau à trier


# Exercice n° 4 : Suivi d'un tri par sélection &#x1F3C6;&#x1F3C6;

On fournit un module [`tris.py`](./tris.py)

__1.__ Etudier le code des fonctions `selection` et `tri_selection` fournies
puis compléter le tableau de suivi de l'algorithme ci-dessous pour le tableau en entrée suivant : `[3,7,5,12,8]`

|  contenu du tableau | i | nombre de comparaisons effectués | `i_min` sélectionné | t[i_min] | échange effectué  ?|
|:----------:|:-----------:|:-----------:|:---------------:|:-----------:|:----------------:|
| `[3,7,5,12,8]`|  0  | 4|  0 (_pas de valeur plus petite_) |  3       | pas d'échange  |
|  [ &nbsp;,&nbsp;  ,&nbsp;  ,&nbsp;  , &nbsp; ]    |   |           |                 |             |                  |
|  [ &nbsp;,&nbsp;  ,&nbsp;  ,&nbsp;  , &nbsp; ]   |      |       |                 |             |                  |
|  [ &nbsp;,&nbsp;  ,&nbsp;  ,&nbsp;  , &nbsp; ]  |   |          |                 |             |                  |

__2.__  Combien de comparaisons ont été effectuées ?  
Ce nombre aurait-il été différent si le tableau avait déjà été trié ?

# Exercice n° 5 : Coût temporel du tri par sélection &#x1F3C6;

_Extrait de Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_

En supposant que le tri par sélection prend 6,8 secondes pour trier 16000 valeurs, estimer le temps qu'il faudrait pour trier un million de valeurs avec ce même tri.


# Exercice n° 6 : Tri Timsort &#x1F3C6;&#x1F3C6;

Le tri utilisé par Python avec les fonctions `sorted` est un algorithme de tri concçu en 2002 et nommé [Timsort](https://fr.wikipedia.org/wiki/Timsort).

__a-__ Créez par compréhension une liste de 15000 nombres entiers tous compris entre 0 et 15000.  

__b-__ A l'aide du module timeit comparez le temps d'exécution des tris suivants pour un même tableau :

* selection
* insertion
* Timsort

_Remarque : il sera judicieux de créer des copies indépendantes de la liste précédente_


Rappel sur l'utilisation de `timeit`

```python
from timeit import timeit

timeit(lambda : fonction_a_tester(), number = 1)
```

__Conclure__


