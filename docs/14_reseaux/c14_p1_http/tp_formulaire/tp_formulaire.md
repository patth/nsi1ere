---
title: "Chapitre 12 : Reseaux"
subtitle : "TP1 :  Formulaires HTML et protocole HTTP"
papersize: a4
geometry: margin=1.5cm
fontsize: 11pt
lang: fr
---

Les formulaires sont essentiels dans les sites web car ils permettent de collecter des données sur l'utilisateur et donc de personnaliser les pages envoyées par le serveur par la suite. 

![Page web du formulaire](./fig/formulaire.jpg)  

Nous allons dans un premier temps concevoir un formulaire simple puis nous simulerons la relation client-serveur en envoyant les données vers un serveur créé sur un poste voisin et analyserons les données transmises liées aux requêtes du protocole HTTP

Récupérez l'archive [`formulaire.zip`](./formulaire.zip) et décompressez_la. 
 
* Vous avez un fichier HTML pré-rempli `formulaire.html`. Pour construire votre formulaire, vous devrez remplacer les zones de commentaires par les balises utiles. 

* Vous avez un fichier CSS lié au fichier HTML du formulaire. Il est rempli, vous pouvez le modifier pour personnaliser votre travail si vous le souhaitez lorsque l'ensemble du travail de la séance sera accompli.

* Vous utiliserez les éléments html présentés ci-après pour completer le fichier `formulaire.html` afin d'obtenir un formulaire similaire à celui présenté dans la __figure 1__.

__2)__  Dans la div qui porte l'id `titre`, vous mettrez "Premier Formulaire" suivi de votre nom. (Cela permettra de reconnaitre votre formulaire.)  

# Conception du formulaire : élements html.

## Element `<form>`  

Il représente une section du document  qui contient des contrôles interactifs permettant à un utilisateur d'envoyer des données à un serveur web[^1]

| Attributs | Descriptions |    
|:-----------:|:------------------------:|     
| `action`  | URL vers laquelle les informations soumises au formulaire sont envoyées|    
| `method` | La méthode HTTP utilisée pour envoyer des données au serveur |  

```html
<!-- Formulaire simple qui enverra une requête GET -->
<form action="" method="get">
  <label for="GET-name">Nom :</label>
  <input id="GET-name" type="text" name="name"/>
  <input type="submit" value="Enregistrer"/>
</form>
```

## Elements `<input>` et `<label>`

L' élément `<input>` a pour but de recueillir les informations d'une entrée effectuée par l'utilisateur. Il est de nature multiple : un chmap de texte, un bouton radio, une case à cocher....  
Il est généralement associé à une étiquette qui permet de le décrire : l'élément `<label>`.

### Les champs textes
#### Champ texte basique

* _Exemple_:    ![champ texte](./fig/input.jpg)

```html 
    <label for = "prenom"> Prénom: </label>  
    <input id="prenom" name ="prenom" type="text" value="prénom"/>  
```  

* pour l'élément `<input>` :

| Attributs | Description |     
|:-----------:|:------------------------:|      
| `type = "text" `| Définit un type input de champ texte. |      
| `name`  | Nom du champ rattaché à la donnée envoyée |    
|`id` | identifiant lié formellement à l'attribut `for` de l'élément `label` |     
| `value` | valeur du champ |    


* pour l'élément `<label>` :

| Attributs | Description |     
|:-----------:|:------------------------:|         
|`for` | identifiant lié formellement à l'attribut `id` de l'élément `input` |     

#### mot de passe

* _Exemple_:    ![mot de passe](./fig/pwd.jpg)

```html 
    <input id="pwd" name ="motdepasse" type="password"/>  
```  


#### email

* _exemple de code associé_ :

```html 
    <input type="email" id="mail" name ="adressemail" />  
```  
 
### Créer un bouton radio

* _Exemple_ :  ![bouton radio](./fig/radio.jpg)

```html 
    <label for = "pere"> Père :</label>  
    <input id="pere" name ="parent" type="radio" value="pere"/>   
    <label for = "mere"> Mère :</label>  
    <input id="mere" name ="parent" type="radio" value="mere" checked="checked"/>   
```

* pour l'élément `<input>` :

| Attributs | Description |     
|:-----------:|:------------------------:|      
| `type = "radio" `| Définit un bouton radio. |      
| `name`  | Tous les boutons radio d'un même groupe doivent avoir le mêmme attribut name |  
| `checked = "checked"`| Permet de sélectionner un des boutons radio du groupe par défaut |   

## Elements `<select>` et `<option>` 

L'élement `<select>` permet de créer une liste de choix le plus souvent déroulante à effectuer parmi plusieurs éléments `<option>` 

* Exemple :  ![liste déroulante](./fig/liste_deroulante.jpg)

```html 
    <label for = "annee"> Année </label>  
    <select id = "annee" name ="annee" value="2019">
        <option value='2019'>2019</option>
        <option value='2020'>2020</option>
    </select>   
```  

## Validation du formulaire

On rencontrera nécessairement un élément `<input>` de type __submit__ à la fin du formulaire qui permettra la validation des données.

```html 
<input id="bouton" type="submit" value="Valider"/>
```

# Transmission des données à un serveur

## Préparation des fichiers stokés sur le serveur

__1)__ Récupérez l'archive [`requete_serveur.zip`](./requete_serveur.zip) et décompressez_la.
Elle contient tous les fichiers nécessaires à la création d'un serveur web via  __Node.js__ et __Express.js__  basés sur Javascript.

_Remarque_ : Node.js est utilisée par exemple par Rakuten,  PayPal, Netflix, Groupon...

![Node.js](./fig/nodejs.png)

Pour des raisons pratiques, vous copierez le dossier `requete_serveur` sur le bureau. Les codes HTML que vous devez compléter se trouvent dans le dossier `views`. Ils portent une extension `.ejs` utilisé par le serveur que vous allez crée mais en les éditant vous constaterez que vous avez à faire à du code HTML.

__2)__ Editez les 2 premiers fichiers `formulaire_get.ejs` et `formulaire_post.ejs` en copiant-collant comme indiqué __le contenu de votre formulaire__ à l'emplacement indiqué et en faisant figurer votre nom dans l'élément d'id titre : enregistrez-les ensuite.

Vous remarquerez que :

* dans le premier fichier `formulaire_get.ejs` : le formulaire transmettra les données via une méthode __GET__ au serveur qui en retour renverra le code présent dans `formulaire_post.ejs`  au client (voir la valeur de l'attribut `action`)  
* dans le second fichier `formulaire_post.ejs` : le formulaire transmettra les données via une méthode __POST__ au serveur qui en retour renverra le code présent dans `reponse.ejs` au client.

## Demarrer le serveur web:

- Ouvrez l'invite de commande Windows: touches `window + R`, vous tapez `cmd` puis validez avec `Entrée`.  

- Placez vous dans le dossier `requete_serveur` présent sur le bureau. On utilisera la commande `cd` suivi du nom du dossier pour se placer dans celui-ci

```bash
> cd Desktop/requete_serveur
```

- Démarrez enfin le serveur avec la commande

```bash
> npm run start
```

Si tout se passe bien vous observerez ceci :

![Démarrage serveur](./fig/serveur.jpg)

## Réaliser une requête sur le serveur web de votre voisin

__1)__ Demander à votre voisin  l'__adresse IP__ de son ordinateur. Elle figure sur le bureau de votre ordinateur
Par exemple _192.168.1.16_

__Notez-la : .................................__

_Remarque_ : on peut la retrouver en ouvrant un autre invite de commande avec `ipconfig`

__2)__ Ouvrez le navigateur Mozilla Firefox puis tapez ensuite sur la touche `F12` afin d'ouvrir les __outils de développement__ : sélectionnez l'onglet `Réseau` et vérifiez que le sous-onglet `Tout` est sélectionné.

__3)__ Dans la barre de navigation, tapez l'adresse IP de votre voisin suivie de __:3000__ c'est le `port d'écoute` qu'on utilisera ici.   Par exemple _192.168.1.16:3000_  

Vous devriez accéder au formulaire créé par votre voisin et vous visualiserez dans l'onglet Réseau toutes les requêtes faites au serveur.


## Analyse des requêtes http.  

__1)__ Combien de requêtes HTTP ont été effectuées lorsque vous vous êtes connecté au serveur ? Quelle est la méthode utilisée pour chacune d'entre elles ?   
.......................

__2)__ Quel contenu est lié à chacune de ces requêtes ?
..........................................................

__3)__ Sur quel Domaine ont été effectuées ces requêtes ?

............................................................

Voici un tableau récapitulant les états des réponses des requêtes (Cette liste reprend les principales réponses du serveur lorsqu'il reçoit une requête du client):  

| Code | Message | Signification |   
|:------------:|:------------:|:-----------:|  
| `200` | __OK__ | Requête traitée avec succès |  
| `304` | __Not modified__ | Document non modifié depuis la dernière requête|     
| `404` | __Not found__ | Ressource non trouvée. |    


__4)__ Quels sont les états des reponses aux requêtes réalisé sur le serveur web de votre voisin?  

.................................................................................. 

__5)__ Rechargez-la page `F5` pour observez le nouvel état de ses requêtes. Commentez  

.............................................................

__Compléter le "Premier formulaire" puis validez-le__ 

__6)__ Regardez dans la barre de navigation. Qu'observez vous dans l'__URL__?  

................................................................................................... 

__7)__ Dans l'onglet Réseau cliquez sur la première requête HTTP en partant du haut (celle permettant d'accéder au document html) et observez le contenu de l'onglet `En-têtes`   

* Où sont placées les données collectées par le formulaire dans l'URL de la requête ? .....................  
 
* Comment ces données sont elles séparées ? .....................   

La méthode `GET` demande une représentation de la ressource spécifiée. Les requêtes GET doivent uniquement être utilisées afin de récupérer des données.  

* Cette méthode est-elle adaptée ici ?...................

__8)__ Vous pouvez visualisez les __en-têtes bruts__ des requêtes faites par le client et des réponses données par le serveur. Recherchez :

* La version du navigateur utilisé par le client ainsi que son système d'Exploitation ...........................
* la version du protocole HTTP utilisée (_HTTP/n_) : ............
* Le logiciel utilisé par le serveur (_server_ ou _X-Powered-By_) : ....................


__9)__ Placez vous du côté serveur et observez ce qui a été envoyé (il faut regarder dans l'invite de commandes de votre voisin). 

__Compléter le "Deuxième formulaire" puis validez-le__ 

__10)__ Regardez dans la barre de navigation. Comparer l'URL de la méthode POST avec celle de la méthode GET?  

.........................................................................................      

__11)__ Cliquez dans l'onglet Réseau sur la première requête http  en partant du haut et regardez à droite dans "En têtes".  

* Quelle est la méthode de la requête?   ..............   

* Expliquez pourquoi la méthode POST est recommandée pour l'envoi d'un formulaire avec des données sensibles?    

..................................................................................................

__12)__ Cliquez sur l'onglet "paramètres". Que constatez-vous?  Peut-on dire que les données sont sécurisées? 
  
............................................................................................................................................  

__13)__ Observez le corps de la réponse (onglet Réponse)
  

__Vous pouvez maintenant arrêter le serveur `CTRL + C` et confirmez par `O + Entrée`__

## Le protocole HTTPS  

Le protocole __HTTPS__ a la même fonction que le protocole HTTP mais les données lors des transferts sont cryptées.  
Pour vous en assurer, loggez vous sur votre compte ENT par exemple.  
Normalement, vous allez avoir une requête POST liée au formulaire de connexion. Cliquez sur cette requête puis observez les paramètres.  Que constatez vous?  

 ..................................................................................................



[^1]: https://developer.mozilla.org/fr/docs/Web/HTML/Element/Form