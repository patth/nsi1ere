---
title: "Chapitre 12 : Réseaux "
subtitle: "PARTIE 2 - Cours: Transmission de données dans un réseau."
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


> 📢 Nous parlons d'un __réseau__ lorsque au minimum deux ordinateurs peuvent échanger directement des informations. (Soit par un réseau cablé/fibré soit par un réseau wifi/lifi)  

# Le réseau local.  

## Eléments du réseau local 
  
Nous allons travailler sur une image d'un réseau local d'une habitation afin de trouver les différents éléments du réseau et leur fonction.  

![Réseau local](./fig/image_reseau_local.jpg)  

* Le __routeur__: Chez nous, le routeur est assimilé à la "box". Cet appareil a pour but de relier votre réseau local au monde extérieur c'est à dire au réseau Internet.  
Il partage une même connexion internet entre différents éléments du réseau local. Il établit donc des "routes" entre les éléments dans une maison ou entreprise et le monde extérieur.  

* Le __commutateur réseau__ (_ou_ __switch__): C'est un boitier comportant plusieurs ports ethernet. Il permet de relier en réseau local plusieurs éléments (ordinateurs ou autres).  
Le switch permet avant tout de répartir l’information de manière « intelligente » au sein du réseau local. Il reçoit des informations d'un émetteur et les envoie vers un destinaire précis. Il réalise cela grâce à une table: la __table MAC__.  

* Les __cables ethernet__: Le débit maximal est de 10 Gbit/s et la fréquence supportée peut aller jusqu'à 250 MHz.  

* Les __cartes réseaux__: les éléments qui sont connectés au switch ou au routeur possède tous une carte réseau. C'est le composant qui permet la communication avec le réseau.
  
## Définitions

### Adresse MAC

> 📢 Tous les appareils connectés possède une adresse physique unique au monde: c'est une adresse __MAC__ (_Media Acces Control_) stockée dans leur carte réseau. On parle aussi d'_adresse Ethernet_.

Une adresse MAC est composée de __6 octets__ soit __48 bits__ et est généralement représentée sous forme hexadécimale. Par exemple : `5E:FF:56:A2:AF:15`  
__Dans le modele OSI, MAC se situe dans la couche 2 : la couche de liaison__

### Adresse IP

> 📢Dans un réseau local chaque périphériphrique connecté posséde un numéro d'identification attribué de façon permanente ou provisoire : c'est l'adresse __IP__ (_Internet Protocol_). On parle aussi d'_adresse Internet_.

Il existe des adresses IP de version 4 sur 32 bits et de version 6 sur 128 bits.  
__Dans le modele OSI, IP se situe dans la couche 3 : la couche réseau.__  

Les adresses IPv4 sont représentées en notation décimale

![Adresse IPV4](./fig/adressip.png)  

## Fonctionnement d'un réseau local  

Si on suppose un réseau local composé de plusieurs ordinateurs et d'un switch. Lorsque l'ensemble des cables sont branchés entre ces ordinateurs et le switch, on dit que le réseau physique est opérationnel.  
Les ordinateurs ne peuvent cependant pas encore communiquer entre eux car ils ne se "voient" pas.  

Dans la mémoire de l'ordinateur (ou élément du réseau), sera stockée une __table ARP__. 

> 📢 Le protocole __ARP__(_Adress Resolution Protocol_) permet à une machine de trouver si elle existe l'adresse MAC d'une autre machine sur le réseau local. La table ARP fait la correspondance entre les adresses IP du réseau local et les adresses MAC. 

__Dans le modele OSI, ARP se situe dans la couche 3 : la couche réseau__

Les ordinateurs envoient des requêtes à l'ensemble du réseau local et en fonction des réponses, ils construisent progressivement leur table ARP. Le but de ce protocole est de cacher aux applications opérant à une couche supérieure l'adresse pysique des machines et de ne leur permettre de manipuler que les adresse Internet

Exemple:  

![Table ARP ordinateur ](./fig/tablearp.png)  

* Pour le switch, il va de la même manière observer les flux et affecter à ces ports les adresses MAC des éléments de son réseau local. 
   
Exemple:  

![Table ARP switch ](./fig/tablemac.jpg)  

Lorsque les liens entre les ports, les adresses IP et les adresses MAC sont réalisées, les ordinateurs peuvent communiquer entre eux. (la construction des tables se fait progressivement.)  

# Internet un réseau de dimension mondiale.  

## Qu'est ce que le réseau internet?  

![Représentation réseau internet](./fig/internet.jpg)  

On pourrait voir internet comme un ensemble de routeurs reliés entre eux qui permettent de connecter les réseaux locaux. les liaisons entre les routeurs sont nombreuses ce qui permet d'avoir de multiples chemins pour aller d'un réseau local A à un réseau local B.  

Ce réseau Internet étant très complexe, il est necessaire d'imposer un adressage précis et un protocole de transport.  

## L’ADRESSE IP Publique 

* Dans le réseau local, une adresse IP locale est attribuée de manière dynamique par le serveur DHCP de la box (routeur qui va permettre la liaison avec internet.) Elle commence souvent par 192.168...  ou 172.16....  

* Lorsque vous vous connectez à internet via un ordinateur, un smartphone..., vous demandez à votre FAI une adresse __IP publique__. 
C'est le __routeur__  qui dispose de cette adresse côté internet : elle est unique dans le monde.[^1]


![IP publique et privées](./fig/ip_routeur.png)  


## Le protocole TCP.

Les protocoles Ethernet et IP permettent la communication de machine à machine mais en pratique ce ne sont pas les machines qui communiquent entre elles mais les programmes qui s'y exécutent  : c'est le rôle du protocole TCP

[Lien vers la vidéo TCP/IP](https://www.youtube.com/watch?v=AYdF7b3nMto)  


> 📢 Le protocole __TCP__ (_Transmission Control Protocol_) est un ensemble de règles qui fixe les conditions de transport des informations découpées en __segments__.  
>  Le protocole TCP utilise le numéro de port pour identifier les applications.  
>  Dans le modèle OSI il correspond à la couche 4 : Transport

Nous allons décrire ce protocole dans le cas d'un échange entre un client et un serveur

### Etablissement d'une connexion

Le protocole TCP nécessite d'abord d'établir une connexion entre 2 machines souvent constitué de trois phases:[^2]  

![Etablissement d'une connexion](./fig/synchro_tcp.jpg)  

* __Le client envoie__ d'abord au serveur un numéro de séquence généré aléatoirement (_dans notre exemple 1010_) dans un paquet TCP particulier (__SYN__)

* __Le serveur qui reçoit__ alors le paquet SYN génére à son tour un numéro aléatoire (_dans notre exemple 3002_) qu'il va renvoyer dans un paquet particulier (__SYN-ACK__) qui contient également le numéro du client incrémenté de 1 (_1011 dans notre exemple_). Ce paquet est à la fois un accusé de réception pour le client et une synchronisation au niveau des numéros d'échange côté serveur et client.

* __Le client qui reçoit__ ce paquet se considère connecté au serveur et envoie un dernier paquet particulier (__ACK__) dans lequel le numéro de séquence serveur aura été incrémenté (3003 dans l'exemple). Le serveur qui reçoit ce paquet se considére à son tour connecté au client.

__Ce processus en trois temps permet ainsi au serveur et au client de se synchroniser en s'échangeant leurs numéros de séquences avant de commencer leurs échanges__

### Encapsulation des données

Une fois la connexion établie, les données à trensmettre sont découpées et encapsulées dans des segments TCP.

Chaque couche du modèle OSI ajoute ses informations dans qu’on appelle un __entête__ (_header_) : c'est le principe de l'__encapsulation__

![Encapsulation des données](./fig/basique_encapsulation.png)  

Description:[^3]

* __1__ L’utilisateur entre une donnée (une url par exemple)
* __2__ Cette donnée va être correctement formatée et envoyée à la couche inférieure (entête HTTP)
* __3__ La couche Transport (ici TCP) concatène à la donnée l’entête qui définit les paramètres de la couche en question comme par exemple le port source et port destination. Une fois l’entête ajouté, le __segment__ TCP (entête + donnée) est envoyé à la couche inférieure
* __4__ La couche Réseau (ici IP) concatène au segment TCP (entête TCP + donnée) l’entête IP avec entre autre les adresses IP source et IP destination. Une fois l’entête ajouté, le paquet IP (entête IP + entête TCP + donnée) est envoyé à la couche inférieure (_on parle aussi de Datagramme IP_)
* __5__ La couche Liaison de données (ici Ethernet) concatène au paquet IP (entête IP + entête TCP + donnée) l’entête Ethernet avec entre autre les adresses MAC source et MAC destination. Une fois l’entête ajouté, la __trame__ Ethernet (entête Ethernet + entête IP + entête TCP + donnée) est envoyé à la couche inférieure
* __6__ La couche Physique (cuivre fibre, fibre optique...) transforme cette trame composée de 0 et 1 numérique en signaux électrique pour l’envoyer sur le câble. La trame quitte la carte réseau du PC


### Récupération de paquets perdus: protocole de bit alterné.  

Nous avons vu que le protocole TCP propose un mécanisme d'accusé de réception afin de s'assurer qu'un paquet est bien arrivé à destination. On parle plus généralement de __processus d'acquittement__. Ces processus d'acquittement permettent de détecter les pertes de paquets au sein d'un réseau, l'idée étant qu'en cas de perte, l'émetteur du paquet renvoie le paquet perdu au destinataire. Nous allons ici étudier un protocole simple de récupération de perte de paquet : le __protocole de bit alterné__.  

Le principe de ce protocole est simple, considérons 2 ordinateurs en réseau : un ordinateur A qui sera l'émetteur des trames et un ordinateur B qui sera le destinataire des trames. Au moment d'émettre une trame, A va ajouter à cette trame un bit (1 ou 0) appelé __drapeau__ (_flag_ en anglais). B va envoyer un accusé de réception (acknowledge en anglais souvent noté _ACK_) à destination de A dès qu'il a reçu une trame en provenance de A. À cet accusé de réception on associe aussi un bit drapeau (1 ou 0).  

La première trame envoyée par A aura pour drapeau 0, dès cette trame reçue par B, ce dernier va envoyer un accusé de réception avec le drapeau 1 (ce 1 signifie "la prochaine trame que A va m'envoyer devra avoir son drapeau à 1"). Dès que A reçoit l'accusé de réception avec le drapeau à 1, il envoie la 2e trame avec un drapeau à 1, et ainsi de suite...  

![Protocole bit alterné](./fig/protocolebitalterne.png)  

_Remarque_ : Le système de drapeau est complété avec un système d'horloge côté émetteur. Un "chronomètre" est déclenché à chaque envoi de trame, si au bout d'un certain temps, l'émetteur n'a pas reçu un acquittement correct (avec le bon drapeau en accusé de reception), la trame précédemment envoyée par l'émetteur est considérée comme perdue et est de nouveau envoyée.

Pour plus d'informations : https://pixees.fr/informatiquelycee/n_site/nsi_prem_bit_alt.html


[^1]: https://abcreseau.blogspot.com/2017/11/ip-fixe-ou-dynamique-de-votre-fai.html

[^2]: https://www.schoolmouv.fr/cours/routage-tcp-/fiche-de-cours

[^3]: https://reussirsonccna.fr/principe-basique-de-lencapsulation/