---
title: "TP : Les fonctions"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

# Exercice n°1 : Fonctions natives et documentation &#x1F3C6;

__1)__ Faites apparaitre la documentation de la fonction `pow()`  
    __a-__ A quoi sert-elle ? Testez-la avec une exemple de votre choix pour le vérifier.  
_Remarque: le symbole `/` dans la documentation des paramètres `(...,/)`  n'est pas à prendre en compte._    
    __b-__ Combien de paramètres au minimum doivent être utilisés ?  

__2)__ Faites apparaitre la documentation de la fonction `abs()`  
    __a-__ A quoi sert-elle ? Testez-la avec une exemple de votre choix pour le vérifier.    
    __b-__ Combien de paramètre(s) doivent être utilisés ?


# Exercice n°2 : Quelques fonctions personnalisées &#x1F3C6;

Pour cet exercice, créez préalablement un script nommé `fonctions_persos.py`.
Pour chacun des cas suivants vous définirez correctement une fonction en n'oubliant pas de :  

* choisir un nombre de paramètres adaptés
* nommer les fonctions et les paramètres de manière non ambigüe
* de créer une docstring associée et de tester.

__1)__ Définir une fonction renvoyant le carré d'un nombre  
__2)__ Définir une fonction renvoyant le perimètre d'un rectangle  
__3)__ Définir une fonction renvoyant la moyenne de 3 notes  
__4)__ 🥇Définir une fonction retournant la moyenne __pondérée__ de 2 notes


# Exercice n°3 : IMC &#x1F3C6;

__1)__ Créer un nouveau script nommé `imc.py`.   

__2)__ En reprenant la définition vue lors de la séance précédente indiquez combien de paramètres sont nécessaires à la définition d'une fonction permettant de calculer l'IMC.

__3)__ Créez la fonction `calcul_imc` correspondante en choisissant des noms de paramètres explicites.  
Vous n'oublierez pas de renseigner la docstring

__4)__ Après avoir sauvegardé votre script, testez-la fonction avec les exemples suivants :

```python
>>> calcul_imc(95, 1.81)
28.997893837184456

>>> calcul_imc(140, 2.04)
33.64090734332949
```

__5)__ On souhaite améliorer cette fonction en donnant une valeur arrondie au dixième du résultat.  
Ecrivez une fonction `calcul_imc_bis` en utilisant la fonction `round` vue en cours (_reprenez la documentation associée si nécessaire_)

```python
>>> calcul_imc_bis(140, 2.04)
33.6
```

# Exercice n°4 : Variables et espaces de noms &#x1F3C6;

Soit le script suivant :

```python
x = 3

def incremente(x) :
    x = x + 1
    x = x + 1
    return x

incremente(x)
```

Visualisez l'exécution du code étape par étape à l'aide __Python Tutor__ :  

http://www.pythontutor.com/visualize.html#mode=edit

__1)__ Combien de variables nommées `x` sont utilisées dans ce programme ? Lesquelles sont locales et globales ?  

__2)__ Expliquez l'instruction `x = x + 1` et indiquez quelles valeurs affefctées aux variables sont modifiées ?   
Faites de même pour la seconde instruction `x = x + 1`   

__3)__ A la fin du programme combien de variables nommées `x` existent encore en mémoire ?  


# Exercice n°5 : Théorème de Pythagore &#x1F3C6; &#x1F3C6;
  
__1)__ Définissez la fonction `calcul_hypotenuse` qui acceptera deux paramètres notés `a` et `b` correspondant aux longueurs des 2 plus petits côtés d'un triangle rectangle (exprimées avec la même unité).   
La valeur de retour de cette fonction sera la longueur de l'hypoténuse.[^1]  

![Théorème de Pythagore](./fig/pythagore.png)

__a)__ De quelles fonctions du module math avez vous besoin pour calculer la longueur de l'hypoténuse ?

__b)__ Arpès avoir rédigé votre code, enregistrez le script sous le nom `hypotenuse.py` et vérifiez votre fonction à l'aide des exemples suivants: 

```python
>>> calcul_hypotenuse(2, 3)
3.605551275463989
>>> calcul_hypotenuse(5, 2)
5.385164807134504
```

__2)__ Le module math possède une fonction nommée `hypot`.  
Importez-la, puis recherchez dans sa documentation son mode d'utilisation.  
Que constatez-vous ?

# Exercice n°6 :  Des fonctions en série &#x1F3C6; &#x1F3C6;

__1)__ Définir une fonction `cube` renvoyant le cube d'un nombre passé en paramètre

__2)__ On rappelle que le volume d'une sphère peut être calculé à l'aide de la formule suivante  $V =\dfrac{4\times \pi \times R^{3}}{3}$

__a-__  Importez `pi` provenant de la bibliotheque `math`. Vous pouvez évaluez sa valeur : 

```python
>>> pi
3.141592653589793
```

__b-__ Définir une fonction `volume_sphere` renvoyant le volume d'une sphère en fonction du rayon passé en paramètre.

__3)__ Proposez une deuxième version de cette fonction `volume_sphere_bis` qui fait appel à la fonction `cube` initialement définie.


# Exercice n°7 : Lancer de dés &#x1F3C6;

__1)__ Beaucoup de jeu de rôles (JDR) utilisent des dés spéciaux avec un nombre de faces adapté. 

![Dé 20 faces](./fig/20-sided_dice_250.jpg)[^2]

Définissez une fonction `dice_20` sans paramètre simulant le lancer d'un dé à 20 faces (le nombre le plus petit étant `1` et les valeurs allant de un en un )

_remarque_ : lors de la définition d'une fonction sans paramètre ou de son appel les parenthèses sont tout de même obligatoires, par exemple :

```python
>>> dice_20():
15
```

__2)__ Ajoutez un commentaire expliquant l'importation réalisée

__2)__ Définissez une fonction `dices_roll` (sans paramètre également) simulant le lancer simultané de 3 dés 20.  
Cette fonction renverra uniquement la somme du lancer : __vous utiliserez bien évidemment la fonction précédente dans cette nouvelle fonction__.  


# Exercice n° 8 : QCM &#x1F3C6;

Indiquez à chaque fois __la bonne réponse__ (l'utilisation de l'ordinateur est interdit):

__Question 1__  On considère la fonction suivante, quelle est la valeur de retour pour `fonction_mystere(4, 2)`?   

```python
def fonction_mystere(a, b):
    return a // b
```

* [ ] 0
* [ ] 2.0
* [ ] 4
* [ ] 2


__Question 2__  On considère la fonction suivante, quelle est la valeur de retour pour `fonction_mystere_bis(9)`?   

```python
def fonction_mystere_bis(a):
    b = 3
    return b + a / 3
```

* [ ] 4
* [ ] 4.0 
* [ ] 6.0
* [ ] 6


__Question 3__  On considère le code suivant, par quoi remplacer `nom_fonction` ?  

```python
from math import sqrt

def pythagore(a, b):
    """ Renvoie la longueur de l'hypoténuse"""
    return nom_fonction(a * a + b * b)
```

* [ ] math.sqrt 
* [ ] sqrt 
* [ ] sqrt.math
* [ ] math


__Question 4__  On considère la fonction `f` suivante, quelle est la valeur de retour pour `f(3, 2, 1, 4)`?    

```python
def f(a, b, c, d):
    a = b
    c = d
    return b ** b + c * d
```

* [ ] 20 
* [ ] 28 
* [ ] 8
* [ ] 9


__Question 5__  On considère le code suivant, quelle est la valeur de `y` à la fin ?    

```python
x = 4
y = 4

def f(x, y):
    x = 4
    y = y + 2
    return x + y

y = y - f(x,y) + x
```

* [ ] -2 
* [ ] -1 
* [ ] 0
* [ ] 1


__Question 6__  On considère le code suivant, quelle est la valeur de `resultat` à la fin ?    

```python
x = 0

def f(n) :
    return n ** 2

def g(x):
    return f(x) + 3

resultat = g(2)
```

* [ ] 0 
* [ ] 3 
* [ ] 5
* [ ] 7


# Exercice n°9 : Autour du cercle 🚀

__1)__ Créer un nouveau répertoire nommé `ex9` et nouveau script à l'intérieur nommé `cercle.py`. 

__a-__ Définissez-y la fonction `perimetre` qui acceptera un seul paramètre `rayon` correspondant au rayon d'un cercle. La valeur de retour de cette fonction sera le périmètre de ce cercle.

Vérifiez votre fonction à l'aide des exemples suivants: 

```python
>>> perimetre(2)
12.566370614359172
>>> perimetre(100)
628.3185307179587
```

__b-__ Toujours dans le même script, définissez la fonction `aire` acceptant un seul paramètre `rayon`.    
La valeur de retour sera l'aire du disque correspondant.

Vérifiez votre fonction à l'aide des exemples suivants:

```python
>>> aire(1)
3.141592653589793
>>> aire(5)
78.53981633974483
```

__2)__ 🥇 Après avoir sauvegardé votre script précédent, fermez le fichier puis ouvrez une nouvelle fenêtre et enregistrez un nouveau script pour l'instant vierge sous le nom `test_module.py` __dans le même répertoire__ que le script précédent.

`cercle.py` peut  être utilisé comme un module indépendant.

Essayez d'importer le module __cercle__ dans votre nouveau script et utilisez les fonctions `aire` et `perimetre` depuis ce nouveau script.

__3)__ Ajoutes des commentaires expliquant l'utilisation de ce module.

[^1]: https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Pythagore
[^2]: [20-sided dice, Photo by Fantasy on the English Wikipedia project](https://commons.wikimedia.org/wiki/File:20-sided_dice_250.jpg?uselang=fr)