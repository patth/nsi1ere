---
title: "Chapitre .. : Recherche dichtomique "
subtitle : "Activité introductive"
---

# Préalable :

Pour cette activité nous allons utiliser un __notebook jupyter__.  
Si ce n'est déjà fait : installer le module __jupyter__ (via Thonny : `Outils\Gérer les paquets`)

# Préparation du notebook :

__1)__ Récupérez tout d'abord l'archive suivante : [notebook_dichotomie.zip](./notebook_dichotomie.zip) et enregistrez-la. Puis, décompressez-la impérativement dans un dossier de votre choix (__notez bien l'emplacement__ et surtout la lettre associée au disque s'il y en a une __C__, __D__ ...).

__2)__ Dans Thonny, ouvrez une __invite de commandes__ : `Outils\Ouvrir la console du système` (_Open system shell_)

Un terminal doit alors s'ouvrir (_si ce n'est pas le cas, enregistrez le script courant sous un nom quelconque puis réouvrez le terminal_)

![Invite de commandes](./fig/cmd.jpg)  

Comme pour le Shell de Python, le terminal attend des instructions qui seront validées par l'appui sur la touche `Entrée`.
Le prompt de python `>>>` est remplacé par le chemin menant au répertoire courant. 

__3)__ 

* Placez vous tout d'abord à la racine du disque où vous avez enregistré le notebook 

Sous windows (si le disque se nomme C)

```bash
c:
```

Sous MacOS ou Linux 

```bash
cd /
```

* Puis lancez l'application jupyter

```bash
jupyter notebook
```

Après quelques instants permettant l'initialisation d'un serveur local jupyter sur votre machine une page web s'affichera ressemblant à ceci 

![Serveur jupyter ](./fig/serveur_jupyter.jpg)  

Retrouvez le fichier `dichotomie.ipynb` présent dans le dossier créé lors de la décompression de l'archive en naviguant dans l'arborescence et exécutez-le en cliquant dessus.

---

--> Si vous ne parvenez pas à lancer le notebook malgré ces explications : utilisez le lien suivant 

[![Notebook Dichotomie](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_dichotomie%2Fdichotomie.ipynb)  

_le temps de chargement peut être parfois être  de 1 à 2 min maximum ....._

---


#  Fonctionnement du notebook

Le carnet est constitué de cellules :

- Certaines contiennent du texte mis en forme, des images, des animations.. 

![cellule texte ](./fig/text_cell.jpg)  

- D'autres sont utilisées comme blocs-notes pour répondre aux questions

![cellule reponse ](./fig/response_cell.jpg)  

- Enfin, les cellules précédées de `Entrée [ ]` sont des cellules exécutables

![cellule python ](./fig/python_cell.jpg) 

  - Toutes les cellules devront être exécuter une par une (et chronologiquement) : sélectionnez la cellule puis appuyez simultanément sur `Ctrl + Entrée`  ou sur cliquer sur le bouton `Exécuter`

-   Lorsque le code est exécuté, l'ordre dans lequel les cellules ont été evaluées apparait entre  `[ ]`

![cellule python entrée ](./fig/python_cell_entree.jpg) 

_Attention, parfois vous aurez à réexécuter des cellules précédentes pour mettre à jour votre code !_

---

SOURCES : 

* Le notebook a été réalisé en grande partie à partir de https://github.com/nsi-acot/continuite_pedagogique_premiere/tree/master/algorithmique/dichotomie