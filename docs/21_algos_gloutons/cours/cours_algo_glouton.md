---
title: "Chapitre 21 : Algorithmes Gloutons "
subtitle: "Cours : Méthode gloutonne"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

Dans beaucoup de problèmes on recherche une solution optimale par exemple : 

* Optimisation de parcours de distribution de marchandises ou de personnes. 
* Optimisation des parcours des machines dans une entreprise. 
* Optimisation d'un planning.
* Optimisation des tracés électroniques sur une plaque de silice.

Après avoir décrit quelques exemples concrets on s'intéressera à une famille d'algorithmes permettant de trouver  des solutions à ces problèmes.  

# Exemples de problèmes concrets.  

## TSP _(Traveling Salesman Problem)_ : le voyageur de commerce[^1]  

![TSP : Traveling Salesman Problem](./fig/car54.jpg) 

C’est sous forme de jeu que William Rowan Hamilton a posé pour la première fois ce problème, dès 1859. Sous sa forme la plus classique, son énoncé est le suivant :  

_« Un voyageur de commerce doit visiter une et une seule fois un nombre fini de villes et revenir à son point d’origine. Trouvez l’ordre de visite des villes qui minimise la distance totale parcourue par le voyageur »_

L'optimisation se fait ici sur un critère précis: __la distance parcourue doit être la plus courte.__  


## Le rendu de monnaie [^2]    

![rendue de monnaie](./fig/monnaie.jpg)  

Dans ce type de problème, un commerçant doit rendre la monnaie à l'un de ses clients avec un ensemble de pièces (ou de billets) d'un système monétaire donné.  

L'optimisation se fait ici sur un critère précis: __le nombre de pièces ou billets rendus doit être le plus petit possible.__  

_Application:_  Optimisation d'une caisse d'un commerce mais aussi des distributeurs automatiques.  

## KP _(knapsack problem)_ :  le remplissage du sac à dos  

![KP : knapsack problem ](./fig/knapsack.png)  

Un des énoncés possibles pour ce problème est le suivant: 

_« Étant donnés plusieurs objets possédant chacun une masse et un prix et étant donnée une masse maximale supportée pour le sac, quels objets faut-il mettre dans le sac de manière à maximiser la valeur totale sans dépasser la masse maximale autorisée par le sac ? »_[^3]  

L'optimisation se fait ici sur : __la valeur totale du sac doit être la plus grande possible.__ (Une contrainte: ne pas dépasser la masse maximale.) 


_Applications:_  

* Dans les systèmes financiers où il faut maximiser les gains avec la contrainte d'une somme au départ en investissant sur des produits financiers.
* Dans la découpe en industrie où il faut découper un maximum de pièces sur une plaque de surface donnée en minimisant les pertes.
* Dans le chargement de cargaisons dans un avion, un bateau, une valise...

# Comment résoudre ces problèmes ?  

La première information importante est que tous ces problèmes possèdent une solution optimale : ils sont solubles.  

## Résolution naïve

La première idée serait de faire une résolution __naïve__ qui consiste à étudier tous les cas possibles et de prendre, en fonction des critères imposés, la solution optimale.  

Prenons l'exemple du voyageur de commerce qu'on personnalise à notre situation.  
Un bus part de Gondecourt et doit réaliser un circuit passant une fois et une seule par les villes suivantes Allennes-les-marais, Carnin et Chemy avant de revenir à Gondecourt.  

![voyager près de Gondecourt](./fig/villes.png)

On dispose du tableau de données représentant la distance en kilomètres entre les villes :  

|   |Gondecourt| Allenes-les-marais | Carnin | Chemy |  
|:----------:|:----------:|:----------:|:----------:|:----------:|  
|Gondecourt|  | 2,8 | 3,6 | 1,8 |  
| Allennes-les-marais| 2,8 |  | 2,5 | 4,1 |  
|Carnin| 3,6 | 2,5 |  | 4,4 |  
|Chemy| 1,8 | 4,1 | 4,4 |  |  

__Nous voudrions trouver le trajet qui est le plus court possible.__  

On dénombre les trajets:  

| | | | | | |  | 
|:----------:|:----------:|:----------:|:----------:|:----------:|:----------:|:----------:|  
| _n°1_ |Gondecourt | Allennes | Carnin | Chemy | Gondecourt | 2,8 + 2,5 + 4,4 + 1,8 = 11,5 |  
|  _n°2_ |Gondecourt | Allennes | Chemy | Carnin | Gondecourt | 2,8 + 4,1 + 4,4 + 3,6 = 14,9 |  
| _n°3_ | Gondecourt | Chemy | Allennes | Carnin | Gondecourt | 1,8 + 4,1 + 2,5 + 3,6 = 12 |  
|  _n°4_ |Gondecourt | Chemy | Carnin | Allennes | Gondecourt | 1,8 + 4,4 + 2,5 + 2,8 = 11,5 |  
|  _n°5_ |Gondecourt | Carnin | Allennes | Chemy | Gondecourt | 3,6 + 2,5 + 4,1 + 1,8 = 12 |  
|  _n°6_ |Gondecourt | Carnin | Chemy | Allennes | Gondecourt | 3,6 + 4,4 + 4,1 + 2,8 = 14,9 |  

Nous pouvons observer que les parcours 4, 5 et 6 sont identiques aux parcours 1,2 et 3. On ne prendra donc pour notre étude que les parcours 1, 2 et 3.  
D'après les calculs, nous observons que le parcours 1 est le plus court.  

La résolution de ce problème est donc simple car il y a peu de villes dans le trajet et donc peu de possibilités de trajets.  
Une résolution naïve qui dénombre l'ensemble des possibilités est donc possible.  

## Que se passe t-il si le nombre de villes dans le parcours augmente ?  

En mathématique, on montre que le nombre de trajets possibles (en ne passant qu'une fois dans chaque ville) se calcule avec la formule:  

$nombre\_de\_trajets = \frac{(nombre\_de\_ villes-1)!}{2}$  

> 📢 __`n!`__ se lit __factorielle de n__   
> C'est le produit des nombres entiers strictements positifs inférieurs ou égaux à n  
> $n!=n\times(n-1)\dots\times1$   

Dans notre exemple précédent, nous avions 4 villes (Gondecourt, Allennes-les-marais, Chemy et Carnin) pour trouver le nombre de trajets,

$nombre\_de\_trajets = \frac{(4-1)!}{2}= \frac{3!}{2}= \frac{3\times2\times1}{2}=3$  
 

Que se passe-t-il si nous avons un nombre plus grand de villes ?  

* _Pour 7 villes_ : $nombre\_de\_trajets = \frac{(7-1)!}{2}= \frac{6!}{2}= \frac{6\times5\times4\times3\times2\times1}{2}=360$  

* _Pour 11 villes_ : $nombre\_de\_trajets = \frac{10!}{2}= \frac{10\times9\times8\times7\times6\times5\times4\times3\times2\times1}{2}=1814400$
  
* _Pour 16 villes_ : $nombre\_de\_trajets = \frac{15!}{2}= 6.5 \times 10^{11}$
  
On voit donc que le nombre de trajets augmentent très rapidement et la résolution naïve devient impossible en un temps raisonnable même avec l'aide d'ordinateurs !  

# L'algorithme glouton.  

## Quand utiliser un algorithme glouton ?  

Les algorithmes gloutons (_greedy algorithm_) sont utilisés pour répondre à des problèmes d'optimisation lorsque la résolution naïve n'est plus possible.   

Nous appliquerons les algorithmes gloutons dans les problèmes suivants:  

* Lorsque le nombre de solutions est grand.
* lorsqu'il est possible d'attribuer une valeur à chaque solution.
* lorsqu'on recherche une solution qui soit bonne (pas forcément la meilleure...)

## Qu'est ce que l'algorithme glouton ?  

> 📢 Un algorithme glouton est un algorithme qui suit le principe de faire, étape par étape, un choix local optimal.  
> Ce choix local est identique à chaque étape.    

_Remarque_: Comme le choix local n'aboutit pas forcement à la meilleure solution globale, on envisage alors des méthodes approchées appelées __heuristiques__.  

> 📢 Les __heuristiques__ sont des méthodes produisant la plupart du temps un résultat acceptable, pas nécessairement optimal, dans un temps raisonnable.[^4]

## Retour sur l'exemple du voyageur de commerce.  

> On choisit d'appliquer une règle entre chaque changement de ville consistant à prendre localement la distance la plus courte entre chaque ville.

|   |Gondecourt| Allennes-les-marais | Carnin | Chemy |  
|:----------:|:----------:|:----------:|:----------:|:----------:|  
|Gondecourt|  | 2,8 | 3,6 | 1,8 |  
| Allennes-les-marais| 2,8 |  | 2,5 | 4,1 |  
|Carnin| 3,6 | 2,5 |  | 4,4 |  
|Chemy| 1,8 | 4,1 | 4,4 | |  

Si on reprend le tableau de données, on voit que partant de Gondecourt, on a Chemy (1,8 distance la plus courte depuis Gondecourt), puis partant de Chemy, on a Allennes-les-marais (4,1 plus courte hormis Gondecourt) puis Carnin (2,5) pour revenir à Gondecourt (3,6).  
Au total, le parcours sera de 12 km.  

On voit que notre choix local qui est d'aller, à chaque fois, à la ville la plus proche ne conduit pas à la solution globale optimale mais cela reste une bonne solution (_la meilleure était de 11,5km_) et à surtout l'avantage d'être rapide à trouver.  
Le choix d'aller à chaque étape à la ville la plus proche est donc une heuristique gloutonne.

## Le rendu de monnaie.  

On choisit le système monétaire euro sans les centimes soit $\{ 1, 2, 5, 10, 20, 50, 100, 200, 500\}$.  
On ne fera pas de différence entre les pièces et les billets. (_Toutes ces valeurs seront supposées être des pièces pour faciliter les explications_)    
Un client achète un article pour 38€, il donne au commerçant 50€.  
__On souhaite que le commerçant rende le moins de pièces possible au client.__

> On choisit donc comme règle locale de donner à chaque fois une pièce qui a la plus grande valeur possible mais qui est inférieure ou égale à la valeur à rendre. 


_Par exemple:_  

* _Première étape_ :  le commerçant doit rendre 12€, il choisit donc de rendre une pièce de 10€ car c'est la valeur la plus grande en dessous de 12.
* _Deuxième étape_:  le commerçant doit encore rendre 2€, il choisit donc de rendre une pièce de 2€. 

Au total, le commerçant aura rendu deux pièces.  

> __Pour le rendu de monnaie dans le système monétaire de l'euro, l'algorithme glouton fournira toujours la solution globale optimale.__ 

_Remarque_: Un système monétaire $\{1, 6, 10\}$ par exemple ne conduirait pas à la solution optimale si on doit rendre 12. On choisira la valeur la plus grande $10$ puis $2\times1$ alors que la solution globale optimale est $2\times6$.

# Ce qu'il faut retenir:  

* On choisit d'utiliser un algorithme glouton dans des problèmes d'optimisation lorsque le dénombrement des possibilités conduit à des calculs trop importants.
* On change d'approche. On ne voit plus le problème dans sa globalité. On recherche une solution locale qui nous semble optimale. Cette solution sera appliquée à chaque étape.
* Comme la solution locale ne conduit pas systématiquement à une solution globale __optimale__, on parle d'heuristique gloutonne.


[^1]: http://www.math.uwaterloo.ca/tsp/gallery/igraphics/car54.html  
[^2]: http://www-sop.inria.fr/members/Nicolas.Nisse/lectures/prepa/6_DynamicProgramming.pdf  
[^3]: https://fr.wikipedia.org/wiki/Probl%C3%A8me_du_sac_%C3%A0_dos  
[^4]: Types de données et algorithmes (C. Froidevaux, M.-C. Gaudel, M. Soria)