---
title: "Chapitre 22 : Algorithmes KNN"
subtitle: "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
--- 

## Exercice 1 : QCM &#x1F3C6;

__1__ On dispose d’une table de données de villes européennes.  
On utilise ensuite l’algorithme des k-plus proches voisins pour compléter automatiquement cette base avec de nouvelles villes.
Ci-dessous, on a extrait les 7 villes connues de la base de données les plus proches de Davos.  

|Ville |Pays |Distance jusqu’à Davos|
|:----:|:----:|:----:|
|Berne |Suisse |180 km|
|Innsbruck |Autriche |130 km|
|Milan |Italie |150 km|
|Munich |Allemagne |200 km|
|Stuttgart |Allemagne| 225 km|
|Turin |Italie| 250 km|
|Zurich |Suisse |115 km|

En appliquant l’algorithme des 4 plus proches voisins, quel sera le pays prédit pour la ville de Davos ?

* [ ] Allemagne
* [ ] Autriche
* [ ] Italie
* [ ] Suisse

__2__ Comment appelle-t-on l'ensemble des données d'entrée qui permettent de construire l'algorithme KNN?  

* [ ] Le jeu de Manhattan
* [ ] Le jeu de classes
* [ ] Le jeu de voisins
* [ ] Le jeu d'apprentissage 

__3__ À quelle catégorie, appartient l’algorithme des k plus proches voisins ?

* [ ] Algorithmes de classification et d’apprentissage
* [ ] Algorithmes de recherche de chemins
* [ ] Algorithmes de tri
* [ ] Algorithmes gloutons

__4__ Nous disposons du code suivant:  

``` python
import math

def f(ax, ay, bx, by):
    return math.sqrt((ax-bx)**2+(ay-by)**2)
```  

`(ax;ay)` et `(bx;by)` étant les coordonnées de deux points. Que peut renvoyer cette fonction ?  

* [ ] Une distance de Manhattan
* [ ] Le nombre de voisins
* [ ] Une distance Euclidienne
* [ ] Les coordonnées du milieu d'un segment

## Exercice 2 : Classifier les iris &#x1F3C6; &#x1F3C6;  

On dispose d'une liste de dictionnaires `datas` fournie dans le fichier [iris.py](./iris.py) regroupant les carctéristiques de trois types d'iris (les Sétosas, les Versicolors et les Virginicas).  
Nous avons pour chaque iris mesuré quatre carctéristiques:  

* La longueur des sépales  
* La largeur des sépales  
* La longueur des pétales  
* La largeur des pétales.  

Voici la structure d'un élément de la liste:  
`{'longueur_sepale': 4.6, 'largeur_sepale': 3.1, 'longueur_petale': 1.5, 'largeur_petale': 0.2, 'Nom': 'Setosa'}`  

__1)__ Donner les classes de cet exercice.  

__2)__ Comment appelle-t-on l'ensemble des données de la liste __donnees_iris__?  

__3)__ Créer la fonction `distance_e` permettant de renvoyer la distance euclidienne entre deux points de données.
Elle acceptera  deux paramètres :  des dictionnaires de même type que ceux dans la liste `datas`.  

_Remarque_ : On généralisera la définition donnée en cours (avec 2 critéres) aux quatres critères numériques pour les iris

Par exemple :

```python
>>> distance(datas[6], datas[7])
4.230839160261237
```

__4)__ Créer le même type de fonction mais en utilisant la distance de Manhattan. 

```python
>>> distance_m(datas[6], datas[7])
2.6457513110645907
```

## Exercice 3 : HARRY POTTER &#x1F3C6; &#x1F3C6; &#x1F3C6;  

Lorsque de jeunes sorciers arrivent à l'école de Poudlard, le Choixpeau magique répartit les élèves dans les différentes maisons (Elles représenteront les classes dans notre exercice):  

* Griffondor
* Serpentar
* Serdaigle
* Poufsouffle

Pour choisir la maison de chaque sorcier, le Choixpeau magique s'appuie sur quatre critères (Ils représenteront les quatre critères des points de données):  

* Leur courage
* Leur loyauté
* Leur sagesse
* Leur malice

Nous disposons d'un fichier [choixpeauMagique.csv](./choixpeauMagique.csv) qui contient les points de données visés par notre expert qui est le ChoixpeauMagique. Ces points de donnée représentent notre __jeu d'apprentissage__.  

A partir de l'ensemble de ces données, nous allons aider Choixpeau magique à trouver la maison de certains jeunes sorciers dont nous avons les caractéristiques suivantes:  

| Nom | Courage | Loyaute | Sagesse | Malice | Maison |  
|:--------:|:--------:|:--------:|:--------:|:--------:|:--------:|  
| Hermione | 8 | 6 | 6 | 6 | ??? |  
| Drago | 6 | 6 | 5 | 8 | ??? |  
| Cédric | 7 | 10 | 5 | 6 | ??? |  

![Choixpeau magique](images/chapeau.png)  

__1)__ Vous allez commencer par recopier la fonction `convertir_csv_dico` qui prend comme paramètre `nom_fichier` qui représente le nom du fichier dont on souhaite extraire les données.

Cette fonction permet de renvoyer une liste de dictionnaires à partir du fichier __choixpeauMagique.csv__.  

``` python  
def convertir_csv_dico(nom_fichier):
   """
   Récupère les données d'un fichier .csv et les fournit une liste de dictionnaires.

   : param str nom_fichier: nom du fichier qui contient les données et a pour extension .csv
   : return : liste de dictionnaires
   : rtype : list(dict)
   """
   file = open(nom_fichier, newline='')
   csv_en_dico = csv.DictReader(file , delimiter = ';')
   liste = []
   for ligne in csv_en_dico :
      liste.append(dict(ligne))
   file.close ()  
   return liste
```  

Verifier que cette fonction qui a pour paramètre le fichier `choixpeauMagique.csv` renvoie:  

```python
[{'Nom': 'Adrian', 'Courage': '9', 'Loyaute': '4', 'Sagesse': '7', 'Malice': '10', 'Maison': 'Serpentar'},  
 {'Nom': 'Andrew', 'Courage': '9', 'Loyaute': '3', 'Sagesse': '4', 'Malice': '7', 'Maison': 'Griffondor'},  
 ....
 ....]
``` 

Chaque sorcier est ainsi représenté par un dictionnaire présent dans la liste fournie en valeur de retour.

__2)__ Vous allez maintenant créer la fonction `calcul_distance` qui a pour premier paramètre `mystere` qui est un jeune sorcier dont on ne connait pas la maison et `sorcier` qui est un jeune sorcier du jeu d'apprentissage. Ces deux paramètres seront des dictionnaires de la forme:  

`{'Nom': 'Adrian', 'Courage': '9', 'Loyaute': '4', 'Sagesse': '7', 'Malice': '10', 'Maison': 'Serpentar'}`  

On utilisera pour cette fonction la distance Euclidienne en la généralisant aux quatre paramètres (Courage, Loyaute, Sagesse et Malice). Cette fonction renverra un tuple qui contient dans l'ordre la distance, le nom du sorcier du jeu d'apprentissage et sa maison.  

Ainsi :

```python
>>> calcul_distance({'Nom': 'Hermione', 'Courage': '8', 'Loyaute': '6', 'Sagesse': '6', 'Malice': '6', 'Maison': '???'},  
                {'Nom': 'Adrian', 'Courage': '9', 'Loyaute': '4', 'Sagesse': '7', 'Malice': '10', 'Maison': 'Serpentar'})

(4.69041575982343, 'Adrian', 'Serpentar')
``` 

__3)__ Créer la fonction `toutes_les_distances` qui prend en paramètre `mystere` c'est à dire un sorcier dont on ne connait pas la classe (La maison). Le paramètre sera de type dictionnaire comme pour le 2.  

La fonction `toutes_les_distances` renverra une liste de tuples (Distance, Nom du sorcier du jeu d'apprentissage, Maison du sorcier) triée par ordre croissant des distances.  

Ainsi, : 

```python
>>> toutes_les_distances({'Nom': 'Hermione', 'Courage': '8', 'Loyaute': '6', 'Sagesse': '6', 'Malice': '6', 'Maison': '???'})

[(4.69041575982343, 'Adrian', 'Serpentar'), (3.872983346207417, 'Andrew', 'Griffondor'), (3.7416573867739413, 'Angelina', 'Griffondor')....]
```

__4)__ Créez la fonction `voisins` qui prend deux paramètres : 

* Le premier est le nombre `k` qui est un entier qui détermine le nombre de voisins choisis pour réaliser la prédiction
* Le second paramètre est le sorcier mystère qui est un dictionnaire comme vu précédement.  

La fonction renverra une liste de  tuples comme la fonction précédente mais qui correspondra aux k voisins les plus proches (il sera nécessaire de faire un tri suivant la distance) 

```python
>>> voisins(5, {'Nom': 'Hermione', 'Courage': '8', 'Loyaute': '6', 'Sagesse': '6', 'Malice': '6', 'Maison': '???'})

[(2.449489742783178, 'Cormac', 'Griffondor'), (3.0, 'Neville', 'Griffondor'), (3.1622776601683795, 'Colin', 'Griffondor'), (3.1622776601683795, 'Dean', 'Griffondor'), (3.3166247903554, 'Milicent', 'Serpentar')]
```

__5)__ Créez enfin la fonction `maison` qui acceptera deux paramètres :

* le nombre de voisins `k` 
* et `liste_voisins` correspondant à la valeur de retour de la fonction précédente.

La fonction renverra un tuple qui comportera en premier la classe du sorcier mystère(sa maison) et en second le pourcentage de voisins parmi les __k__ qui ont cette classe.  

Ainsi :

```python
>>> k_voisins = voisins (5, {'Nom': 'Hermione', 'Courage': '8', 'Loyaute': '6', 'Sagesse': '6', 'Malice': '6', 'Maison': '???'})
>>> maison(k_voisins)
('Griffondor', '80.0%')
```

__6)__ Pour terminer, trouver la maison de chaque sorcier mystère donné dans le tableau.  


## Exercice 4 : Reconnaissance de caractères &#x1F3C6; &#x1F3C6; &#x1F3C6;  

Dans cet exercice nous allons nous intéresser aux algorithmes d'apprentissage supervisé via la bibliothèque __scikit-learn__.  
C'est une bibliothèque spécialisée dans l'apprentissage automatique et elle est actualisée très régulièrement.  


__1)__ On commencera donc par installer la bibliothèque __sklearn__ via __Thonny__ dans le gestionnaire de plug-ins.  

Nous aurons besoin aussi de la bibliothèque de gestion d'image __PIL__ et d'une bibliothèque de gestion de tableau __numpy__. Si elles ne sont pas installées, on procèdera comme pour __sklearn__.  

Le but de cet exercice est de créer un algorithme __knn__ de reconnaissance de caractères. Pour cela, la bibliothèque __sklearn__ nous fournira un jeu de points de données. C'est une base de données représentant des chiffres manuscrits de 0 à 9. Il y a 1797 références ce qui constitue un nombre suffisament important pour réaliser un algorithme d'apprentissage.  

![Chiffres manuscrits](images/mnist.jpeg)

Nous pourrons diviser les points de données de sklearn en deux parties:  

* Le jeu d'apprentissage  
* Le jeu de tests.  

__2)__ Dans un premier temp, récupérez l'archive [reconnaissance_caracteres.zip](./reconnaissance_caracteres.zip) elle contient des images à tester et le script à compléter.

__3)__ Expliquons les imports effectués  en début de script.

* `load_digits` est la base de données comportant l'ensemble des points de données :  `sklearn.datasets` comporte bien d'autres bases de données.
* `train_test_split` nous permettra de scinder la base de données en __jeu d'apprentissage__ et __jeu de tests__.
* `KNeighborsClassifier` est une classe qui va nous permettre de créer un objet algorithme KNN.

__4)__ Nous allons utiliser des images extérieures à la base de données. Ces photos auront toutes pour caractéristiques 8x8 c'est à dire 8 pixels en largueur et 8 pixels en hauteur soit 64 pixels.    

Les images fournies ont les mêmes dimensions que les photos de la base de données de __sklearn__ mais ils faut les stocker sous le même format que celle de la base de données. On dispose de la fonction suivante:  

``` python  
def image(file):
   im = Image.open(file)
   tab = np.array(im)
   return tab
```

* `Image.open()` est une instruction qui permet d'ouvrir un fichier image. On affectera le contenu à la variable `im`.  
* La bibliothèque `numpy` nous permet de créer des tableaux. On pourra ainsi stocker l'ensemble des informations dans la variable `tab` 

__a-__ Choisissez une image dans le répertoire `mystere` decompressé : le paramètre `file` correspond au chemin vers cette image.  
Affichez la contenu de la variable `tab` et vérifiez qu'elle est constituée de huit listes. 

Chacune des 8 listes contient elle-même huit listes de trois éléments.  
_Par exemple (1ère liste correspondant à l'image `mystere_0.png`)_

```python
>>> tab = image('mystere_0.png')
>>> tab[0]
array([[  0,   0,   0],
       [  1,   1,   1],
       [ 54,  54,  54],
       [112, 112, 112],
       [171, 171, 171],
       [108, 108, 108],
       [  0,   0,   0],
       [  0,   0,   0]], dtype=uint8)
```

Cela représente les 64 pixels et chaque pixel contient trois valeurs qui correspondent au codage RVB c'est à dire une valeur pour la couleur __rouge__, une valeur pour la couleur __verte__ et une pour le couleur __bleue__ Ce qui représente au total 512 valeurs 

__b-__ On souhaite garder une liste de 64 éléments étant chacun la somme des trois valeurs liées aux couleurs RVB.  
__Modifier puis compléter-la fonction précédente__:  

``` python  
def image(file):
   im = Image.open(file)
   tableau = np.array(im)
   liste = []
   #Code à compléter  
   return np.array([liste])  
```  

Pour vérifier votre travail :

```python
>>> image('mystere_1.png')
array([[ 21,  87, 633, 684, 672,   6,   0,   0,  66, 738, 753, 714, 732,
        177,   0,   0, 714, 735, 435, 720, 741, 174,   0,   0, 663, 312,
        216, 747, 747, 297,   0,   0,   0,  57, 279, 708, 729, 297,  63,
          0,   0,  72, 354, 723, 738,  93,   3,   0,   6,  99, 297, 741,
        750, 150,   0,   0,   0, 123, 339, 705, 678, 252,   0,   0]])
```

__5)__ La fonction `algo_knn` fournie qui prend pour paramètre `mystere`. Ce paramètre est une chaîne de caractères comportant le nom de l'image mystère avec son extension.    
Un jeu d'images mystères se trouve dans le dossier, vous pourrez tester votre code avec ces images puis dans la question suivante, vous pourrez créer vos propres images.  

La structure de cette fonction est complètement donnée : nous allons juste comprendre chaque ligne de code afin de se familiariser avec la bibliothèque __scikit-learn__.  

```python
base_de_donnees = load_digits() 
donnees = base_de_donnees.data
etiquettes = base_de_donnees.target
```

* La variable `donnees`  va contenir les données des 1797 images de la base de données `base_de_donnees` sous forme de tableaux. Nous allons aussi créer une variable `etiquettes` qui va contenir l'ensemble des noms des images.  

* On scinde la base de données en deux parties (le jeu d'apprentissage et le jeu de tests) avec l'objet  `train_test_split`. 

```python
x_entrainement, x_test, y_entrainement, y_test = train_test_split (donnees, etiquettes , test_size= 0.25)
```

`train_test_split` prend trois paramètres qui sont: `donnees`, `etiquettes` et `test_size` qui fixe la proportion des données qui vont être utilisées pour faire les tests. Souvent on prend test_size = 0.25 ce qui équivaut à 25% des données pour les tests.   
Il nous renvoie un tuple que nous dispersons.  

Maintenant que nous avons récupéré les données et les étiquettes des classes et que nous avons divisé notre base de données en un jeu d'apprentissage et un jeu de tests, il faut créer un objet qui va nous permettre de réaliser l'algorithme KNN.  

* Nous allons créer l'objet `KNN` en instanciant la classe `KNeighborsClassifier`.  
Cette classe prend un paramètre `k` qui est le nombre de voisins pris en compte pour réaliser la prédiction. Comme nous avons 1797 références, nous fixerons arbitrairement la valeur de k à 200. Vous pourrez changer cette valeur par la suite.  

```python
KNN = KNeighborsClassifier(200)
```

Cet objet KNN possède plusieurs méthodes.  

* Nous allons utiliser la méthode `fit` qui va réaliser la phase d'entrainement.   
Cette méthode prend deux paramètres `x_entrainement` et `y_entrainement`. (`x_entrainement` correspond aux données de la base de données réservées à l'entrainement et `y_entrainement` aux étiquettes correspondantes)  

```python
KNN.fit(x_entrainement, y_entrainement)
```  

* Nous allons maintenant préparer l'image mystère qu'on souhaite tester en appelant la fonction qu'on a réalisé au 2.  

```python
test_image = image(mystere)
```  

* On renvoie le résultat avec le niveau de confiance. Pour cela, on utilise la méthode `predict` appliquée à l'objet `KNN`.  
Elle prendra pour paramètre `image_externe`.  Cela renverra la classe de l'image mystère.  

```python
return (KNN.predict(test_image), str(round(KNN.score(x_test, y_test)*100,1))+'%')
```

La méthode `score` appliquée à l'objet `KNN` acceptera elle deux paramètres, `x_test` et `y_test`. Cela renverra un nombre compris entre 0 et 1 qui fixera le taux de confiance. 

Vérifier avec les images mystères fournies que votre code donne la prédiction et le taux de confiance.  

Par exemple :

```python
>>> algo_knn('mystere_0.png')

(array([0]), '89.8%')
```

* array([0]) : signifie chiffre 0  
* 89.8% : Taux de confiance de 89.8%  

__6)__ Vous pouvez créer vos propres images et tester.  

Pour cela, vous devez utiliser un logiciel de dessin comme __GIMP2__.    
Vous créez une nouvelle image.    
Vous fixez les dimensions à largeur 8px et hauteur 8px.    
Vous zoomez pour voir la zone de travail.   
Vous mettez le fond en noir (avec l'__outil Remplissage__)    
Avec la gomme vous tracez le chiffre que vous voulez (modifiez préalablement la taille de la gomme en doublecliquant sur l'__outil Gomme__).    
Vous __Exportez vers__ sous le format `.png`   

Testez dans votre programme.  

