---
title: "Installer un shell UNIX"
---

# Plusieurs manières de tester un shell UNIX

![cas de figure](./fig/diagramme_shell_unix.jpg)

Choisissez la possibilité en fonction des outils et du matériel à votre disposition : 

## cas n°1 (Distribution Linux ou macOs) 

* [ouvir un terminal sous mac](https://fr.wikihow.com/ouvrir-le-Terminal-sur-un-Mac)
* [ouvir un terminal sous Ubuntu ou équivalent ](https://fr.wikihow.com/ouvrir-une-fen%C3%AAtre-de-terminal-dans-Ubuntu)

L'invite de commandes devrait ressembler à ceci :

![Terminal Ubuntu](./fig/terminal_ubuntu.png)


## cas n° 2 et 3 : solution choisie __mobaXterm__ (rapide)

[télécharger et installer mobaXterm](https://www.youtube.com/watch?v=E1rMooFomHM) : regardez la video jusqu'à 1min06

L'invite de commandes devrait ressembler à ceci :

![Terminal mobaXterm](./fig/invite_mobaxterm.png)


## cas 3 : solution choisie installation de __WSL__ (_installation plus longue_)

[Tutoriel pour activer WSL](https://www.youtube.com/watch?v=CyG16N3GJWo)  : regardez la video jusqu'à 5min52

L'invite de commandes devrait ressembler à ceci :

![Terminal WSL](./fig/invite_wsl.png)

  
## cas 4 : __webLinux__ : un navigateur suffit     

[Cliquez sur ce lien ](http://weblinux.univ-reunion.fr) : et attendez que la Weblinux soit opérationnelle

L'invite de commandes devrait ressembler à ceci :

![Terminal WebLinux](./fig/invite_weblinux.png)

## cas 5 : installer une __machine virtuelle__

Si vous avez du temps devant vous, un bon ordinateur et une bonne connexion: vous pourrez installer un système de type Unix complet sans danger pour votre ordinateur.

* [Configuration de votre ordinateur](https://lecrabeinfo.net/virtualbox-installer-windows-linux-dans-une-machine-virtuelle.html#prerequis) : ne lire que le chapite prérequis

* [la méthode détaillée](./installation_VM_et_lubuntu.md) : installation de __VirtualBox__ et d'une distribution __Lubuntu__

L'invite de commande après lancement du terminal est semblable au cas n°1

# CONSEILS 

Solutions à privilégier par ordre décroissant :

* __1__ uniquement si vous avez déjà l'OS
* __4__ : dans tous les cas
* __2__ : l'émulateur est très simple à installer (il existe même une version portable que vous pouvez enregistrer sur un clé USB)
* __3__ : si vous avez Windows 10 et encore un peu de place sur votre disque dur, le sous système linux n'est pas très compliqué à installer et offre toutes les possibilités en terme de lignes de commande qu'offrirai une distribution Linux
* __5__ : Une machine virtuelle vous permet sans risque de tester un autre système d'exploitation sur votre machine sans modifier votre système et donc sans danger. Cependant c'est la solution qui est la plus gourmande en terme de ressources pour votre ordinateur. 


[__RETOUR AU TP en suivant ce lien__](../tp_shell.md)