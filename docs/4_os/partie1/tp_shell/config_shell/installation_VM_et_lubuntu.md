---
title: "Installer un système d'exploitation libre"
subtitle: "Utilisation VirtualBox et Lubuntu"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Télécharger VirtualBox  

![logo VirtualBox](./fig/virtualbox_logo.png)  

* [Télécharger en suivant ce lien](https://www.virtualbox.org/wiki/Downloads)  
Choisir la bonne version dépendant de  votre système d'exploitation (certainement Windows  ou OS X pour les possesseurs de MAC)

* Télécharger également le pack d'extension

# Télécharger Lubuntu

![logo Lubuntu](./fig/lubuntu.png)  

[Télécharger en suivant ce lien](https://lubuntu.fr/)  : choisissez la version 64 bits (1Go environ)


# Installer VirtualBox

* Dans l'installeur : valider sans rien changer (`Suivant`) pour toutes les étapes puis `Installer`

* Installer également après le pack d'extension

# Créer une nouvelle machine virtuelle

* Cliquer sur Nouvelle ( ou CTRL + N)

* Nom : __Lubuntu__ (Le dossier de la machine  est du type _C:\Users\<Votre_Nom>\VirtualBox VMs_ ), Type Linux, Version Ubuntu(64 bits)

Pour la suite aucun changement normalement :

* Taille de la mémoire : _ne rien changer_
* Disque dur : créer un disque virtuel maintenant `Créer`
* Type de fichier de disque dur :VDI `Suivant`
* Stockage sur disque dur physique : Dynamiquement alloué `Suivant`
* Emplacement du fichier et taille : _C:\Users\<Votre_Nom>\VirtualBox VMs\Lubuntu\Lubuntu.vdi_ `Créer`

Cliquer sur `configuration`
* Général/ Onglet Avancé 
-  Presse papiers partagé : __Bidirectionnel__
-  Glisser-Déposer : __Bidirectionnel__

# Installer Lubuntu sur la machine virtuelle

![Démarrage VM](./fig/demarrage_vm.png)  

* Sélectionner Démarrer
* Choisissez le disque de démarrage : cliquer sur l'icône de Dossier 
* Cliquer sur l' icône Ajouter et sélectionner l'image iso de Lubuntu précédemment téléchargée (_nom du type lubuntu-18.04.2-desktop-amd64.iso_) ---`Choisir`
* Validez avec `Démarrer`
* Language : Français (sélectionner avec les fléches directionnelles)
* Installer Lubuntu
* Disposition du clavier : Français (ne pas changer) `Continuer`
* Mise à jour et autres logiciels : Installation minimale, Télécharger les mises à jour pendant l'installation `Continuer`
* Type d'installation : Effacer le disque et installer Lubuntu (_pas d'inquiétude c'est le disque de la machine virtuelle_) `Installer maintenant`  
 --> Confirmation "Faut il appliquer les changements ?"  `Continuer`

* Où êtes vous ? Paris `Continuer`
* Qui êtes vous ?  nom utilisateur/ mot de passe :  lubuntu / lubuntu (_Attention au pavé numérique, il est surement désactivé_)  `Continuer`

Laisser l'installation se poursuivre. (15 min environ)......

* Installation terminée : `Redémarrer maintenant`
* Please remove the installation medium : Appuyer sur Entrée

La machine virtuelle redémarre

Une fois la boite de dialogue d'ouverture de Session apparue, vous pourrez vous connecter  
_Rappel : Attention au pavé numérique, il est surement désactivé_
