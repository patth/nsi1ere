---
title: "Chapitre 8 : Parcours d'une chaîne et recherche séquentielle"
subtitle : "Cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Une chaîne de caractères est une séquence d'éléments pouvant être parcourue à l'aide d'une boucle.  

# Itération sur les indices

On se rappelle que les caractères d'une chaîne sont accessibles par leur indice.  
Par exemple pour la chaîne `s = 'python'`

|s[0]|s[1]|s[2]|s[3]|s[4]|s[5]|  
|:---:|:---:|:---:|:---:|:---:|:---:|  
|'p'|'y'|'t'|'h'|'o'|'n'|  

Nous pouvons donc utiliser la fonction `range` pour parcourir notre chaîne caractère par caractère en utilisant leur indicede position respectif.  

```python
>>> chaine = "NSI"
>>> for i in range(len(chaine)):
        print(chaine[i])
 
 N
 S
 I
```

* La variable de boucle nommée `i` correspond à l'indice : dans cet exemple sa première valeur sera __0__.     
* Le deuxième paramètre __len(chaine)__ correspond aux nombres de caractères dans la chaine c'est à dire 3.  
* La variable `i` prendra successivement les valeurs entieres générés par `range` c'est à dire 0, 1 et enfin 2 : ils correspondent aussi aux indices des caractères dans  `chaine`).

💾 _Attention_ : si les paramètres de `range` sont mal choisis une __exception__ est levée et affcihée dans la Console !

```python
>>> for i in range(4):
        print(chaine[i])

 N
 S
 I
 Traceback (most recent call last):
   File "<pyshell>", line 2, in <module>
 IndexError: string index out of range
```

L'exception est ici `IndexError` (_erreur d'indice_) accompagnée d'une bréve d'explication string `index out of range` (_indice de chaîne en dehors de l'intervalle_)

> 📢 Quand Python détecte une erreur dans votre code, il lève une __exception__.  
> _Vous en avez sans doute rencontrer quelques unes_ : `IndentationError`, `ImportError`, `NameError`, `IndexError`, `TypeError`...


# Itération sur les caractères

La parcours d'une séquence est une opération très fréquente en programmation, on peut faciliter l'écriture de la boucle précédente en itérant non pas sur les indices des caratères mais sur les caractères eux-mêmes.  

```python
>>> chaine = "NSI"
>>> for car in chaine:
        print(car)

 N
 S
 I
```

__Cette fois-ci la variable de boucle est `car` et, la séquence n'est plus numérique mais elle correspond à la chaîne elle-même.__  

* La chaîne de caractères est parcourue de gauche à droite caractère par caractère.  
* La variable de boucle prendra successivement la valeur de ces caractères.  

Comme vous pouvez le constater, cette structure est beaucoup plus compacte, plus lisible et ne pourra pas jamais lever l'exception précédente.    
Cependant, en procédant ainsi les indices des caractères ne sont pas accessibles : tout dépend donc de l'information dont on a besoin !

# Parcours séquentiel 

La recherche d'un caractère dans une chaîne ou plus généralement la recherche d'une chaine dans un texte, le comptage du nombre d'occurence d'un caractère etc... sont des situations très courantes en programmation. De nombreux algorithmes ont été développés dans le but d'améliorer les performances de ce type de recherche.
Nous allons présenter le plus simple d'entre eux.

> 📢 Le __parcours séquentiel__ consiste à parcourir les éléments de la séquence (intégralement ou en partie) un par un successivement dan l'ordre des indices.

## Cas de la recherche d'un caractère dans une chaîne

On souhaite déterminer si un caratère est présent ou non dans une chaine

__Algorithme de recherche linéaire:__

```
Entrées: chaine (la chaine de caractères de longueur n) 
         car (le caratère  recherché)
Sortie : vrai ou faux

1:  resulat ← faux  
2:  POUR i allant de 0 à n-1 faire
3:    SI car = chaine[i] alors:
4:      resultat ← vrai
5:      RENVOYER resultat
6:  RENVOYER resultat
```

## Coût temporel d'un algorithme

Afin d'évaluer l'efficacité d'un algorithme on a pour habitude de comptabiliser le nombre d'opérations élémentaires effectuées par cet algorithme.   
Détaillons les coûts pour chaque opération : arithmétiques, logiques ou affectations nécessaires à l'exécution de la recherche linéaire précédente.

|                                  |type d'opération| coût temporel| nombre de fois| 
|:--------------------------------|:--------------:|:-------------:|:-------------:|
|1:  resulat ← faux               | affectation    |$c_{1}$       |    1          |
|2:  POUR i allant de 0 à n-1 faire| incrémentation |$c_{2}$       |    au minimum 0 au maximum n  |
|3: &nbsp;&nbsp;     SI occ = chaine[i] alors:      |  comparaison   |$c_{3}$       |     au minimum 0 au maximum n|
|4:   &nbsp; &nbsp; &nbsp; &nbsp;      resultat ← vrai        |  affectation   |$c_{4}$       |   au minimum 0 au maximum n      |

> 📢 Parmi les opérations élémentaires certaines ont un coût temporel beaucoup plus important que d'autre. On peut en général négliger les affectations et les incrémentations.

On dégage de cette analyse deux cas limites :

* _Temps d'exécution minimal_ (meilleur des cas : le premier élément est celui recherché):   

$T_{min}(n=1)=c_{1}+1 \times c_{2} + 1 \times c_{3} + 1 \times c_{4}$   

Si on s'intéresse uniquement aux __comparaisons__ : il n'y en a qu'`une`! 

* _Temps d'exécution maximal_ (pire des cas : c'est le dernier élément de la chaîne qui est recherché):   
  
$T_{max}(n)=c_{1}+c_{2}n+c_{3}n+c_{4}n=c_{1}+(c_{2}+c_{3}+c_{4})n$  

Si on s'intéresse aux __comparaisons__, il y en a `n`.

Dans les deux cas on constate que le temps d'éxécution peut être exprimé sous la forme
$T(n)= a \times n+b$ soit une fonction linéaire du nombre d'éléments à parcourir séquentiellement

> 📢 __L'algorithme de recherche séquentielle a un coût temporel linéaire avec la taille de la séquence à traiter__  

On en conclut que en moyenne, le temps de recherche double lorsque la longueur de la chaîne double. Cette recherche est aussi appelée __recherche linéaire__ ou encore __recherche naïve__.
