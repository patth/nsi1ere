---
title: "Chapitre 8 : Parcours d'une chaîne et recherche séquentielle"
subtitle : "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice n°1: Recherche de caractère &#x1F3C6;  

__1)__ Proposer une fonction Python `recherche` acceptant deux paramètres traduisant l'algorithme de recherche linéaire proposé en cours et tester-la sur différents exemples.

__3)__ Python propose une solution encore plus simple pour répondre à ce problème.
Testez les deux instructions suivantes

```python
>>> 'n' in 'nsi' 

>>> 'f' in 'nsi'
```


# Exercice n°2: Compter les caractères &#x1F3C6; &#x1F3C6; 

__1)__ Ecrire un algorithme qui compte le nombre de fois qu'un caractère figure dans la chaîne : vous utiliserez une recherche linéaire. 

__2)__ Définissez une fonction `compter_caractere` acceptant deux paramètres et traduisant en Python l'algorithme précédent.


# Exercice n°3: QCM &#x1F3C6; 

_(Extraits des QCM E3C diffusables)_ 

__1)__ On considère la fonction suivante :

```python
def trouverLettre(phrase,lettre):
    indexResultat = 0
    for i in range(len(phrase)):
        if phrase[i]== lettre:
            indexResultat=i
    return indexResultat
```

Que renvoie l'appel `trouverLettre("Vive l’informatique","e")` ?

* [ ] 3
* [ ] 4
* [ ] 18
* [ ] 'e'


__2)__ Quelle valeur permet de compléter l’affirmation suivante : « Le nombre d’opérations nécessaires pour rechercher un élément séquentiellement dans un tableau de longueur n est de l’ordre de … » ?

* [ ] 1
* [ ] $n$
* [ ] $n^{2}$
* [ ] $n^{3}$

__3)__ On définit :

```python
def traite(chaine,a):
    nouvelle_chaine = ""
    for k in range(len(chaine)):
        if chaine[k] != a:
            nouvelle_chaine = nouvelle_chaine + chaine[k]
    return nouvelle_chaine
```

Quelle est la valeur renvoyée par l'appel traite("histoire","i") ?

* [ ] "hstore"
* [ ] "ii"
* [ ] "histoire"
* [ ] "


# Exercice n°4  Découper une chaîne &#x1F3C6; &#x1F3C6;  

Définir une fonction `premier_mot(chaine)` qui renvoie le premier mot d’une chaîne de caractère. 

_Par exemple_  

```python
>>> chaine = 'samedi soir, je vais au cinéma'
>>> premier_mot(chaine)

 'samedi'
```

_Consignes_ : __vous n'avez pas le droit d'utiliser la méthode `find()` : une recherche séquentielle est donc conseillée__


# Exercice n°5: Palindrome &#x1F3C6; &#x1F3C6; 

_(Trés inspiré de http://www.emmanuelmorand.net/informatique/PTSI-1516/PTSI1516CoursInfo06.pdf)_ 


Un palindrome est une chaîne de caractères qui est identique qu'on la lise de gauche à droite ou de droite à gauche.   _Par exemple_: "été", "ressasser", "radar", "kayak" sont des palindromes.

L'algorithme suivant permet de tester si une chaîne de caratères est un palindrome

```
Entrée : chaine (_une chaine de n caratères_)

i ← 0
j ← n - 1
TANT QUE i <= j faire :
    SI chaine[i] = chaine[j] alors :
        i ← i + 1
        j ← j - 1
    SINON :
        RENVOYER Faux
RENVOYER Vrai
```

__1)__ Décrire au moyen d’un tableau indiquant l'évolution des valeurs des variables le fonctionnement de l’algorithme précédent pour la chaîne de caractères `'sauras'` puis pour `'radar'`.    
__2)__ Montrer que $j-i$ est un variant de la boucle et en déduire qu'elle se termine.  
__3)__ Prouver que l'algorithme palindrome produit le résultat attendu   
en utilisant l'invariant $i+j = longueur(chaine)-1$   
__4)__ Implémenter en Python cet algorithme  


# Exercice n°6: Verlan  🚀 

Écrire une fonction `verlan(mot)` qui prend en argument un mot et renvoie le mot écrit à l’envers.    

_Par exemple_  

```python
>>> verlan('nez')

 'zen'
```

_NOTE_ : Le programme écrit ici ne réalise pas du "vrai" verlan mais inverse simplement l'ordre des caractères dans la chaîne

_AIDE_ : l'instruction `range` peut accepter un pas négatif  

```python
>>> for i in range(10,3,-1):
        print(i, end=" ")
 
 10 9 8 7 6 5 4 
```

# Exercice n°7: Anagrammes  🚀

> Une anagramme est un mot constitué des mêmes lettres qu’un autre en respectant leurs nombres d’occurrences. 
> Par exemple marion est une anagramme de romain, ainsi que de manoir

__1)__ Ecrire un algorithme qui compare deux chaînes de caractères et vérifie si ces chaînes sont des anagrammes.

__2)__ 🥇 Vous définirez un predicat `sont_anagrammes` traduisant en Python l'algorithme précédent. 

_AIDES_ : vous pourrez utiliser

*  l'opérateur `in` comme vu dans l'exercice 1     
*  la méthode `count`  
   https://docs.python.org/fr/3/library/stdtypes.html?highlight=count#str.count   
  









