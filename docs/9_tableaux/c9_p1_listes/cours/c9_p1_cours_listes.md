---
title: "Chapitre 9 : Une structure de donnée construite, les tableaux "
subtitle: "PARTIE 1 Cours - Listes Python"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

> 📢 Une __séquence__ est une collection ordonnée d'objets qui permet d'accéder à ses éléments par leur numéro de position dans la séquence.

La structure de donnée __tableau__ appelée __LISTE__ en Python est un exemple de séquence.  

# La structure de donnée TABLEAU 

## Qu'est-ce qu'un tableau ?

Lorsqu'il y a plusieurs variables à déclarer et que ces variables sont étroitement liées, il est préférable de les regrouper dans une structure de donnée plus complexe. Ainsi, au lieu d'écrire :  

```python
>>> ville_1 = 'Bordeaux'      
>>> ville_2 = 'Lille'    
>>> ville_3 = 'Paris'  
```

On pourra regrouper ces données dans un __tableau__ :

```python
>>> villes = ['Bordeaux', 'Lille', 'Paris']
```

- On remarque que les trois villes sont regroupées dans une même structure.    
- Il n'y a __qu'un__ nom pour accéder à l'ensemble de la collection : `villes`  
    
Cette séquence étant ordonnée, comme pour les chaînes des caractères : on pourra facilement accéder aux éléments du tableau en utilisant leur indice de position respectif.

```python
>>> villes[1]
 'Lille'
```

> 📢 __Indices de position :__  
> Pour une séquence nommée `tableau` les indices de position varient de `0` à `len(tableau) - 1`

## Comment déclarer un tableau  

En général, ce qu'on appelle _tableau_ dans n'importe quel langage de programmation est une collection d'élèments tous de même type et de longueur fixe établie lors de leur création. 

On pourra donc créer des tableaux de plusieurs manière

* __En énumérant toutes les valeurs entre crochets séparées par des virgules__   

```python
>>> notes = [8, 10, 12]
```

* __Pour de grands tableaux on pourra utiliser l'opérateur de duplication  `*`__

```python
>>> tableau = [0] * 10
>>> tableau
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
```

Dans l'exemple précédent, ceci aura pour effet de créer un tableau de taille 100

```python
>>> len(tableau)
 10
```

# Les spécificités des tableaux en Python

Le langage Python a développé quelques particularités en ce qui concerne les tableaux :  

- Les tableaux en Python s'appellent des __listes__.  
- Le langage Python permet un agrandissement ou un rétrecissement par la _droite_ d'une liste.   

Toutes ces particularités nous montrent que ce que Python appelle des __listes__ ne sont ni des listes (_qui en réalité sont des structures différentes dans d'autres langages_) ni des tableaux :  mais des structures de type construit spécifiques à Python. 

## Déclarer des listes en python : d'autres méthodes

* __Il est possible de créer une liste vide en utilisant les crochets `[]` ou encore le constructeur `list()`:__    

```python
>>> liste = []
>>> liste_2 = list()
```

On peut vérifier le type de ces deux structures ainsi que leur taille :

```python
>>> type(liste)
 <class 'list'>
>>> len(liste_2)
 0
```

* __On peut également transformer les valeurs fournies par l'instruction `range` en liste en une seule opération :__  

```python
>>> nombres = list(range(6))
>>> nombres
 [0, 1, 2, 3, 4, 5]
```

## Mutabilité des listes

> 📢 Les listes en Python sont des objets __mutables__ c'est à dire modifiables. Ce n'est par exemple pas le cas d'une chaîne de caractères qui une fois créée ne peut être modifiée.

```python
>>> tableau = ['N', 'T', 'I'] # une liste est modifiable
>>> tableau[1] = 'S'
>>> tableau
 ['N', 'S', 'I']

>>> chaine = 'NTI'
>>> chaine[1] = 'S'
 Traceback (most recent call last):
   File "<pyshell>", line 1, in <module>
 TypeError: 'str' object does not support item assignment
```

### Modifier des éléments grâce à leur indice

Une fois un tableau construit, on peut toujours le _remplir_ avec des valeurs de son choix aux indices existants.

```python
>>> tableau = [0] * 10 # création d'un tableau de taille 10 ne contenant que des 0
>>> tableau[0] = 2
>>> tableau[8] = 26
>>> tableau
 [2, 0, 0, 0, 0, 0, 0, 0, 26, 0]
```

### Phénomènes d'alias

Prenons un exemple dans lequel nous allons créer une liste `tableau` non vide puis nous allons l'affecter à une autre liste `tableaubis`:

```python
>>> tableau = [0, 1, 2, 3, 4, 5]
>>> tableau_bis = tableau
```

Nous allons vérifier si les variables `tableau` et `tableaubis` font référence au même espace mémoire. Pour cela, nous allons utiliser la fonction `id()` qui donnera l'__identifiant mémoire__ des listes.

```python
>>> id(tableau)
 57778904
>>> id(tableau_bis)
 57778904
```

Les variables  `tableau` et `tableau_bis` référencent la même liste qui se trouve à l'emplacement mémoire dont l'identifiant est _57778904_.  

On peut le vérifier avec [Python Tutor](http://www.pythontutor.com/visualize.html#code=tableau%20%3D%20%5B0,%201,%202,%203,%204,%205%5D%0Atableau_bis%20%3D%20tableau%0Atableau_bis%5B5%5D%20%3D%2010%20%23%20on%20modifie%20le%20dernier%20%C3%A9l%C3%A9ment%20de%20tableau_bis%0Aprint%28tableau_bis%29%0Aprint%28tableau%29&cumulative=false&curInstr=4&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

![emplacement mémoire](./fig/tableaubis.png)

Nous allons vérifier que si nous modifions la liste `tableau`, la liste `tableaubis` est aussi modifiée.  

```python
>>> tableau_bis[5] = 10 # on modifie le dernier élément de tableau_bis
>>> print(tableau_bis)
 [0, 8, 2, 3, 4, 10] 
>>> print(tableau)
 [0, 8, 2, 3, 4, 10]
```

Le changement réalisé sur `tableau` l'est aussi sur `tableau_bis` !

> 📢 Lorsqu'on effectue la copie d'une liste par simple affectation on ne crée pas une nouvelle liste en mémoire.  
> Les deux noms attribués sont des __alias__ référençant la même liste.  
> Toute modification apportée à l'un des alias modifiera l'autre car ils pointent vers le même emplacement mémoire

### Comment réaliser une copie de liste indépendante ?  

Pour réaliser une copie de liste qui sera affectée sur un autre espace mémoire, on peut importer la bibliothèque `copy`.

```python
>>> from copy import copy
>>> tableau = [0, 1, 2, 3, 4, 5]
>>> tableau_bis = copy(tableau)   
>>> print(id(tableau))
 57778904
>>> print(id(tableau__bis))
 63775343
```

> 📢 La fonction `copy` du module `copy` __réalise une copie de liste sur un autre emplacement mémoire__ ce qui rend les deux listes indépendantes.

On peut le vérifier avec [Python Tutor](http://www.pythontutor.com/visualize.html#code=from%20copy%20import%20copy%0Atableau%20%3D%20%5B0,%201,%202,%203,%204,%205%5D%0Atableau_bis%20%3D%20copy%28tableau%29%0Aprint%28id%28tableau%29%29%0Aprint%28id%28tableau__bis%29%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

![Copipe indépendante de liste](./fig/copieliste.png)


## Ajouter des éléments à la fin d'une liste Python 

* __On peut utiliser la méthode `append()` qui ajoute un élément après le dernier.__

```python
>>> tableau = [1, 2, 3]
>>> tableau.append(4)
>>> tableau
 [1, 2, 3, 4]
```

On voit dans cet exemple que la valeur 4 a bien été ajoutée comme __dernier élément__ de la liste.  

* __Par concaténation (opérateur `+`) de tableaux plus petits__  

```python
>>> tableau_concatene = [1, 2, 3] + [4, 5]
>>> tableau_concatene
 [1, 2, 3, 4, 5]
```

## Retirer des éléments d'une liste  

* __On peut utiliser la méthode `pop()`__  

```python
>>> tableau = [0, 1, 2]
>>> tableau.pop()
 2
>>> tableau
 [0, 1]
```

__Attention l'utilisation de cette méthode renvoie une valeur (le dernier élément) et en même temps modifie la liste__

* __On peut supprimer un ou plusieurs éléments du tableau avec la fonction `del()` si on connait leurs indices.__    

```python
>>> tableau = ['A', 'B', 'C', 'D']
>>> del(tableau[2])
>>> tableau
 ['A', 'B', 'D']
```

Ici, l'élément d'indice 2 dans le tableau a été supprimé.

_Remarque_ : bien d'autres méthodes existent, à vous de chercher....  

https://docs.python.org/fr/3/tutorial/datastructures.html


