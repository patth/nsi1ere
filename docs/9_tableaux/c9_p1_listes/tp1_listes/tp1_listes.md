---
title: "Chapitre 9 : Une structure de donnée construite, les tableaux "
subtitle: "TP1 : Découverte des listes python"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---  

# Exercice 1 : QCM   &#x1F3C6; 
(_Quelques questions sont extraites des QCM E3C diffusables_)

__1)__  Quelle instruction valide crée une liste python vide?   

* [ ] `list = liste()` 
* [ ] `liste =[]`
* [ ] `liste = tab`
* [ ] `tab = {}` 

__2)__ Après l'affectation suivante :  

```python
alphabet = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ]
```

Quelle est l'expression qui permet d'accéder à la lettre E ?

* [ ] `alphabet.E`
* [ ] `alphabet['E']`
* [ ] `alphabet[4]`
* [ ] `alphabet[5]`

__3)__  Quel est le contenu du tableau `joueurs_98` après les instructions suivantes :   

```python
joueurs_98 = ['Pogba', 'Dugarry', 'Lizarazu']
joueurs_98[0] = 'Zidane'
```  

* [ ] `['Zidane', 'Pogba', 'Dugarry', 'Lizarazu']`
* [ ] `['Zidane']`
* [ ] `[Zidane, 'Dugarry', 'Lizarazu']`
* [ ] `['Zidane', 'Dugarry', 'Lizarazu']` 


__4)__ Quel est le contenu de `nombres` après les instructions suivantes :  

```python
nombres = [1] * 5
nombres[3] = 1
```  

* [ ] `[1, 1, 1, 1, 1]`
* [ ] `[0, 0, 1, 0, 0]`
* [ ] `[5, 5, 3, 5, 5]`
* [ ] `[5, 5, 1, 5, 5]`


__5)__  Quel est le contenu de `liste` après les instructions suivantes : 

```python
liste = list(range(1,5))
liste[1] = 1
```  

* [ ] `[1, 2, 3, 4]`
* [ ] `[0, 1, 2, 3, 4]`
* [ ] `[1, 1, 3, 4]`
* [ ] `[1, 1, 3, 4, 5]`


# Exercice 2 : Débuter avec les listes &#x1F3C6; &#x1F3C6; 

__1)__ Créer une liste nommée `serie` contenant tous les nombres entiers compris entre 2 et 5 (bornes incluses) de deux manières :

* En utilisant l'opérateur de duplication puis en modifiant les valeurs à l'aides des indices
* A l'aide de l'instruction `range`

__2)__ Définir une fonction sans paramètre `cree_liste_1` qui renvoie la liste `[0, 1, 2]`. (Exercice très simple !)

__3)__ Définir une fonction `cree_liste_n` avec un paramètre `n` (_de type entier_). La fonction renverra une liste contenant par ordre croissant les n premiers entiers naturels. 

__4)__ Définir une fonction `lancer_2_des` sans paramètre, elle renverra une liste contenant 2 nombres aléatoires compris entre 1 et 6 (simulant le lancer successif de deux dés 6).

__5)__ La fonction `choice` du module `random` permet de choisir aléatoirement un élément dans une séquence.  
__a-__ Réaliser l'importation nécessaire et consulter la documentation de la fonction `choice`  
__b-__ Définissez une fonction `tas` acceptant comme paramètre une liste `liste`. La fonction renverra un élément de la liste tiré aléatoirement.

__6)__ 🥇 Définir une fonction `liste_parite` qui prend deux paramètres : 

* un entier `n` 
* une chaine de caractères `parite`.  
 
Si la chaîne fournie est `'pair'`, alors la fonction renvoie une liste des premiers entiers pairs jusque n.  
Si la chaîne fournie est `'impair'`, alors la foncion renvoie une liste des premiers entiers impairs jusque n.   
Sinon : la liste renvoyée est vide.  

Par exemple : 

```python
>>> liste_parite(10, 'pair')
[0, 2, 4, 6, 8, 10]
>>> liste_parite(10, 'impair')
[1, 3, 5, 7, 9]
```


# Exercice 3 : Mutabilité des listes &#x1F3C6; &#x1F3C6; 

(_Extrait de Eduscol, Ressources pour NSI_)  
__PYTHON TUTOR__ : http://pythontutor.com/visualize.html#mode=edit

__1)__  Que vaut `s` après ces instructions ? (__Expliquer puis visualiser l'exécution avec python tutor__)

```python
t = [1, 2, 3]
s = t
t[0]= 5 
```

__2)__  Que vaut `s` après ces instructions ? (__Expliquer puis visualiser l'exécution avec python tutor__)

```python
def truc(t):
    t.append(0)

t = [4]
s = [1]
truc(s)
```

__3)__  Que vaut `s` après ces instructions ? (__Expliquer puis visualiser l'exécution avec python tutor__)

```python
from copy import copy
t=['a', 'b', 'c']
s = copy(t)
del(t[1])
```

__4)__ Que se passera-t-il après ces instructions ? (__Expliquer__)

```python
s = 'abce'
s[3]='d'
```

# Exercice 4 : Effet de bord &#x1F3C6;&#x1F3C6;

_(extraits de : "Numériques et sciences informatiques", Balabonski, Conchon, Filiâtre, Nguyen)_

__1)__ Définissez une fonction `echange(tab, i, j)` qui échange dans le tableau `tab` les éléments aux indices `i` et `j`. 

* vous devrez utiliser une varaible servant de stockage temporaire pour la valeur déplacée.
* Cette fonction n'a pas de valeur de retour, mais un __effet de bord__ : elle modifie la liste passée en paramètre.

__2)__ Testez votre fonction par exemple :

```python
>>> tab = ['a', 'b', 'c', 'd']
>>> echange (tab, 1, 3)
>>> tab
 ['a', 'd', 'c', 'b']
```

# Exercice 5 : Analyse de code &#x1F3C6;

Tout d'abord, __sans tester avec Thonny__, pour chacun des cas suivants : __expliquer__ l'erreur commise puis __corriger-la__.  Notez enfin le type d'__Exception levée__ en testant.    

__1)__  

```python
liste = []
liste[0] = 'a'
```

__2)__  

```python
liste = [1, 5, 6, 0]
x = 3 / liste.pop()
```

__3)__  

```python
liste = [0, 1, 2, 3]
liste = liste + 4
```

__4)__  

```python
films= ['Un nouvel espoir', "L'Empire contre-attaque", 'Le Retour du Jedi']
for i in range(4):
    print(films[i])
```

__5)__  

```python
films= ['La Menace fantôme', "L'Attaque des clones", 'La Revanche des Sith']
for film in len(films):
    print(film)
```

