---
title: "Chapitre 9 : Une structure de donnée construite, les tableaux "
subtitle: "PARTIE 2 Cours - Parcours séquentiel"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Le parcours séquentiel d'un tableau a un __coût linéaire__ à l'instar de celui d'un chaîne de caractères. Illustrons-le avec quelques exemples.

# Recherche d'un nombre d'occurences

Combien de fois une donnée est-elle présente dans un tableau ?

```
Entrées: tab (un tableau)
         cle (l'élèment à rechercher)
Sortie : le nombre d'occurences de cle dans tab

1:  occ ← 0  
2:  POUR chaque element dans tab faire :
3:      SI element = cle alors :
4:          occ ← occ + 1
5:      fin SI
6:  fin POUR
7:  RENVOYER occ
```

L'algorithme naïf consiste à comparer chaque élément présent dans le tableau à l'élément rechercher. Pour un tableau de taille _n_ on effectuera donc dans tous les cas n comparaisons (_ligne 3 de l'algorithme_).    
Comme on l'a vu dans le chapitre précédent, la comparaison est l'opération qui est la plus significative en terme de côut temporel, on peut donc dire que le coût de cet algorithme est linéaire.

Illustrons-le en :

```python
from timeit import timeit

code = """
N = 50  # taille du tableau
tab = N * [1]

def recherche(tab, n) :
    occ = 0
    for e in tab :
        if e == n :
            occ = occ + 1
    return occ

recherche(tab, 2)
"""

temps = timeit(stmt=code, number = 100000) / 100000
```

La méthode `timeit` du module __timeit__ permet de mesurer le temps d'exécution du code fourni (_100000 fois pour réaliser une moyenne_). Avec une machine identique voici quelques résultats confirmant nos propos : 

|_taille du tableau_| _temps moyen de la recherche ($\mu s$)_ |
|:---:|:---:|
|50|3,66|
|100|6,95|
|200|15,2|
|400|25,0|
|1000|60,3|


# Recherche d'un extremum

On souhaite par exemple rechercher un maximum dans un tableau de nombres.

```
Entrées: tab (un tableau de nombres dont les indices vont de 0 à n - 1) 
Sortie : le plus grand nombre présent dans le tableau

1:  maximum ← tab[0]    // on choisit arbitrairement le premier élément du tableau
2:  POUR chaque element dans tab faire :
3:      SI element > maximum alors :
4:          maximum ← element
5:      fin SI
6:  fin POUR
7:  RENVOYER maximum
```

Là encore la méthode naïve consiste a parcourir le tableau élémént par élément et à chaque étape comparer l'élément étudié avec la valeur du maximum conservé. Le coût de cette recherche est linéaire.  


# Utilisation d'un accumulateur

On peut vouloir enfin additionner les nombres présents dans un tableau.

```
Entrées: tab (un tableau de nombres) 
Sortie : la somme des nombres présents dans le tableau

1:  somme ← 0    // somme est un accumulateur
2:  POUR chaque element dans tab faire :
3:      somme ← somme + element
4:  fin POUR
5:  RENVOYER somme
```

Ce type d'algorithme permet par exemple, avec une petite modification, d'effecteur une moyenne. Comme dans les autres cas la parcours est linéaire.

