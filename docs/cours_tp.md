
* Vous pouvez accéder aux versions numériques des cours et des TP en cliquant sur les noms correspondants

* Les icônes ![pdf](./fig/pdf.png) permettent d'accéder à la version pdf du cours.
  
* Pour certains cours une version __notebook__ (interactive) est également proposée en cliquant sur les images ![my binder](https://mybinder.org/badge_logo.svg).  
Après chargement du notebook vous pouvez exécuter chaque cellule de code en la sélectionnant puis en utilisant la combinaison de touches `ctrl`+`Entrée`

---

# Chapitre 1 : Découverte du langage Python

## PARTIE 1 : Manipuler les nombres  

* [Cours :  Manipuler les nombres](./1_decouverte_python/python1_nombres/cours1_manipuler_nombres.md)  [![pdf](./fig/pdf.png)](./1_decouverte_python/python1_nombres/cours1_manipuler_nombres.pdf)
[![Notebook Manipuler les nombres](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_nombres%2Fmanipuler_nombres.ipynb)


* [TP1 :  Nombres](./1_decouverte_python/python1_nombres/tp1_nombres/tp1_nombres.md)


## PARTIE 2 : Fonctions 

* [Cours :  Les fonctions](./1_decouverte_python/python2_fonctions/cours2_fonctions.md)   [![pdf](./fig/pdf.png)](./1_decouverte_python/python2_fonctions/cours2_fonctions.pdf) 
[![Notebook Fonctions](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_fonctions%2Ffonctions.ipynb)


* [TP2 :  Fonctions](./1_decouverte_python/python2_fonctions/tp2_fonctions/tp2_fonctions.md)


## PARTIE 3 : Chaînes de caractères

* [Cours :  Les chaînes de caractères](./1_decouverte_python/python3_strings/cours3_strings.md)   [![pdf](./fig/pdf.png)](./1_decouverte_python/python3_strings/cours3_strings.pdf)
[![Notebook chaînes de caractères](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_strings%2Fstrings.ipynb)


* [TP3 :  Chaînes de caractères](./1_decouverte_python/python3_strings/tp3_strings/tp3_strings.md)  

---

# Chapitre 2 : Représentations des nombres entiers

## PARTIE 1 : Les bases de numération 

* 🎬 [Le langage binaire](https://www.youtube.com/watch?v=VRdp_vaNRoY&feature=youtu.be)  

* [Cours :  Bases numériques](./2_entiers/c2_p1_naturels/cours1_bases.md)  [![pdf](./fig/pdf.png)](./2_entiers/c2_p1_naturels/cours1_bases.pdf)
[![Notebook Bases numériques](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_bases%2Fbases.ipynb)  
  
* [TP1 :  Les bases](./2_entiers/c2_p1_naturels/tp1_bases/tp1_bases.md)  

## PARTIE 2 : Entiers relatifs 

* [Cours :  Entiers relatifs ](./2_entiers/c2_p2_relatifs/cours2_entiers_relatifs.md)   [![pdf](./fig/pdf.png)](./2_entiers/c2_p2_relatifs/cours2_entiers_relatifs.pdf)


* [TP : Entiers relatifs ](./2_entiers/c2_p2_relatifs/tp_relatifs/tp_relatifs.md)

---

# Chapitre 3 : Boucles non bornées

* 🎬 [L'histoire d'Al-Khwarizmi](https://www.youtube.com/watch?v=QbZms6RgLRE)  

* [Cours :  Boucles tant que et algorithmes ](./3_boucles_while/c3_while_algo.md)   [![pdf](./fig/pdf.png)](./3_boucles_while/c3_while_algo.pdf)  
[![Notebook Boucles tant que et algorithmes ](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_while%2Fboucles_while.ipynb)

  
* [TP :  Les boucles tant que](./3_boucles_while/tp_while/tp_boucles_while.md)  


* 💻[Pour aller plus loin...](https://python.iutsf.org/lecon-4-les-boucles/) : Rubrique _"Des exercices pour s'entrainer"_ à partir de _"Liste des multiples d'un entier"_
  
---

# Chapitre 4 : Systèmes d'exploitation et logiciels libres

PARTIE 1 : Initiation au shell

* [Activité découverte ](./4_os/partie1/ac_intro/os_intro.md)
* [Cours  ](./4_os/partie1/cours/cours_os.md)  [![pdf](./fig/pdf.png)](./4_os/partie1/cours/cours_os.pdf)
* [TP : shell Unix et arborescence ](./4_os/partie1/tp_shell/tp_shell.md)
* 💻 [Terminus : Un jeu d'aventure en ligne pour vous familiariser avec les lignes de commandes ](http://luffah.xyz/bidules/Terminus/)

---

# Chapitre 5 : Opérateurs booléens et structures conditionnelles 

* [Cours : Opérateurs booléens et conditionnelles ](./5_booleens_conditions/cours_booleens_conditions.md)  [![pdf](./fig/pdf.png)](./5_booleens_conditions/cours_booleens_conditions.pdf)  [![Notebook Entiers relatifs et réels](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_conditions%2Fbooleens_conditions.ipynb)  


* [TP : Opérateurs booléens et conditionnelles ](./5_booleens_conditions/tp_booleens_conditions/tp_booleens_conditions.md)  
  
---

# Chapitre 6 : Architecture des ordinateurs    

## PARTIE 1 : Portes logiques et circuits combinatoires  

* [Cours fonctions boolennes et portes logiques](./6_architecture/c6_p1_portes_logiques/cours_portes_logiques.md)  [![pdf](./fig/pdf.png)](./6_architecture/c6_p1_portes_logiques/cours_portes_logiques.pdf)  

* [TP Circuits combinatoires](./6_architecture/c6_p1_portes_logiques/tp_portes_logiques/tp_portes_logiques.md)  

## PARTIE 2 : Modèle de von Neumann  

* [Cours Modèle de von Neumann](./6_architecture/c6_p2_von_neumann/cours_vonneumann.md)  [![pdf](./fig/pdf.png)](./6_architecture/c6_p2_von_neumann/cours_vonneumann.pdf)  

* 🎬 [La 1ère carte perforée](https://www.youtube.com/watch?v=MDQHE0W-qHs)   

* 🎬 [La vie de Jon von Neumann(documentaire ARTE)](https://www.youtube.com/watch?v=c9pL_3tTW2c)  _Regarder surtout entre 39:12 et 46:00_  
  
*  [Exemple d'utilisation de la machine débranchée M999](./6_architecture/c6_p2_von_neumann/exemple_utilisation_m999/exemple_m999.pdf)  

* [TP : Architecture et langage assembleur](./6_architecture/c6_p2_von_neumann/tp2_assembleur/tp2_assembleur.md)    

---

# Chapitre 7 : Boucles bornées  

*  [Cours :  Boucles for ](./7_boucles_bornees/cours_for.md)  [![pdf](./fig/pdf.png)](./7_boucles_bornees/cours_for.pdf)  [![Notebook Boucles for](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_for%2Fboucles_for.ipynb)  


*  [TP ](./7_boucles_bornees/tp_for/tp_for.md)  
  
* 🐢[Pour aller plus loin : Défie la Tortue !](./7_boucles_bornees/challenges_turtle/challenges_turtle.md)  

---

# Chapitre 8 : Recherche séquentielle 

* [Cours :  Recherche linéaire ](./8_recherche_sequentielle/c8_recherche_sequentielle.md)  [![pdf](./fig/pdf.png)](./8_recherche_sequentielle/c8_recherche_sequentielle.pdf)  [![Notebook Boucles for](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_recherche_sequentielle%2Frecherche_sequentielle.ipynb)  


* [TP : Parcours des chaînes ](./8_recherche_sequentielle/tp_recherche_chaines/tp_recherche_seq.md)  

* 💻[Coûts temporels comparés ](./8_recherche_sequentielle/comparaisons_operations.py)

--- 

# Chapitre 9 : Une structure de donnée construite, les tableaux  

## PARTIE 1 : Listes Python

*  [Cours :  Listes Python ](./9_tableaux/c9_p1_listes/cours/c9_p1_cours_listes.md)  [![pdf](./fig/pdf.png)](./9_tableaux/c9_p1_listes/cours/c9_p1_cours_listes.pdf)  [![Notebook Listes Python](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_liste_1%2Fnb_liste_1.ipynb)  


*  Comprendre la mutabilité des listes avec Python Tutor    
    * 🎓 [1er exemple : sans utilisation de copy ](http://pythontutor.com/visualize.html#code=def%20copieruneliste%28%29%3A%0A%20%20%20%20liste%20%3D%20%5B%200,%201,%202,%203,%204,%205%5D%0A%20%20%20%20listebis%20%3D%20liste%0A%20%20%20%20liste.append%286%29%0A%20%20%20%20listebis.pop%28%29%0A%20%20%20%20print%28id%28liste%29%29%0A%20%20%20%20print%28id%28listebis%29%29%20%20%20%20%20%20%20%20%20%20%20%20%0A%0Acopieruneliste%28%29%0A%0A%23%20On%20voit%20bien%20que%20liste%20et%20listebis%20pointent%20vers%20le%20m%C3%AAme%20emplacement%20m%C3%A9moire.%20%0A%23%20Ils%20agissent%20sur%20la%20m%C3%AAme%20liste.&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)  
   
    * 🎓 [2ème exemple : avec copy ](http://pythontutor.com/visualize.html#code=from%20copy%20import%20copy%0Adef%20copieruneliste%28%29%3A%0A%20%20%20%20liste%20%3D%20%5B%200,%201,%202,%203,%204,%205%5D%0A%20%20%20%20listebis%20%3D%20copy%28liste%29%0A%20%20%20%20liste.append%286%29%0A%20%20%20%20listebis.pop%28%29%0A%20%20%20%20print%28id%28liste%29%29%0A%20%20%20%20print%28id%28listebis%29%29%0A%20%20%20%20%20%20%20%20%20%20%20%20%0Acopieruneliste%28%29%0A%0A&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)    
  

*  [TP : Découverte des listes ](./9_tableaux/c9_p1_listes/tp1_listes/tp1_listes.md)


## PARTIE 2 : Parcours de listes

*  [Cours :  Parcours séquentiel ](./9_tableaux/c9_p2_parcours_listes/cours/c9_p2_parcours_listes.md)  [![pdf](./fig/pdf.png)](./9_tableaux/c9_p2_parcours_listes/cours/c9_p2_parcours_listes.pdf)  [![Notebook Parcours de listes ](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_liste2%2Fparcours_listes.ipynb) 

*  [TP : Parcours des listes ](./9_tableaux/c9_p2_parcours_listes/tp2_parcourslistes/tp2_parcourslistes.md)


## PARTIE 3 : Listes imbriquées et compréhensions

*  [Cours :  Compréhensions de listes et tableaux à 2 dimensions ](./9_tableaux/c9_p3_comprehension_2d/cours/c9_p3_cours_comprehension_listes2d.md)  [![pdf](./fig/pdf.png)](./9_tableaux/c9_p3_comprehension_2d/cours/c9_p3_cours_comprehension_listes2d.pdf)  [![Notebook Compréhensions de listes et tableaux à 2 dimensions ](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_comprehension_2d%2Flistes_3.ipynb)  

*  [TP : Compréhensions de listes et structures imbriquées ](./9_tableaux/c9_p3_comprehension_2d/tp3_listes/tp3_listes.md)


---

# Chapitre 10 : Technologies du web 1, langages HTML et CSS 

* [Prérequis (niveau débutant) ](./10_html_css/prerequis_html_css.md)    

* [Activité niveau avancé ](./10_html_css/html_css_niveau2.md)  

* 🎓 [__Progresser en HTML et CSS__ : exercices guidés et solutions fournies ](http://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/html/cours_html_css_nsi/accueil_cours_html_css_nsi.html)

---

# Chapitre 11 : Spécification et tests des programmes 

* [Cours ](./11_specification_tests/c11_specification_tests.md)   [![pdf](./fig/pdf.png)](./11_specification_tests/c11_specification_tests.pdf)   [![Notebook Listes Python](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_tests%2Fspecification_tests.ipynb)   

* [TP ](./11_specification_tests/tp_spe_tests/tp_spe_tests.md)  

---

# Chapitre 12 : Les tris

* [Activité débranchée : Introduction aux algorithmes de tris ](http://www.nileb.net/nsi/algo01/balance.html)  

* [TP n°1 : tri par insertion ](./12_tris/tp1_tri_insert/tri_insertion_intro.md)[![Notebook Listes Python](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=tri_insertion%2Ftp_tri_insertion.ipynb) 

* [Cours : les algorithmes de tris ](./12_tris/c12_tris_cours.md)   [![pdf](./fig/pdf.png)](./12_tris/c12_tris_cours.pdf)

* [TP n°2 : Comparaisons de tris ](./12_tris/tp2_comparaisons_tris/tp2_comparaisons_tris.md)  


---
# Chapitre 13 : Interface Homme-Machine (IHM) sur le Web

* [Découverte du Javascript ](./13_ihm_web/c13_p1_js/c13_decouverte_js.md) [![pdf](./fig/pdf.png)](./13_ihm_web/c13_p1_js/c13_decouverte_js.pdf) 

* [Cours : Programmation événementielle  ](./13_ihm_web/c13_p2_evenementiel/c13_evenementiel.md) [![pdf](./fig/pdf.png)](./13_ihm_web/c13_p2_evenementiel/c13_evenementiel.pdf)

* [TP : Evenements en javascript](./13_ihm_web/c13_p2_evenementiel/tp_evenements/tp_evenements.md) 


---
# Chapitre 14 : Réseaux

## PARTIE 1 : Relation client/serveur et protocole HTTP

* [TP : Formulaires et protocole HTTP](./14_reseaux/c14_p1_http/tp_formulaire/tp_formulaire.md)  

* 🎬 [The Internet: HTTP & HTML ](https://www.youtube.com/watch?v=kBXQZMmiA4s)  

* [Cours 1 : Relation client/serveur sur le Web](./14_reseaux/c14_p1_http/cours/c14_cours_http.md) 

* [Exercices ](./14_reseaux/c14_p1_http/exercices/c14_exos_client_serveur.md)

## PARTIE 2 : Transmission de données dans un réseau

* [Cours 2 : Transmission des données dans un réseau ](./14_reseaux/c14_p2_tcp/cours/c14_cours_tcp.md) 

* [TP1 : Premières requêtes sur les réseaux](./14_reseaux/c14_p2_tcp/tp1_filius/tp1_filius.md)  

* 🎬 [The Internet: Packets, Routing & Reliability ](https://www.youtube.com/watch?v=AYdF7b3nMto&t=5s) 

* [TP2 : Protocole TCP](./14_reseaux/c14_p2_tcp/tp2_tcp/tp2_tcp.md) 

---

# Chapitre 15 : Tuples

* [Cours : les n-uplets ](./15_tuples/c15_tuples_cours.md)  [![pdf](./fig/pdf.png)](./15_tuples/c15_tuples_cours.pdf)  [![Notebook Tuples](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_tuples%2Ftuples.ipynb)  


* 🎬 [Les tuples : video complements de cours ](https://youtu.be/Kw8UIavg8Os)    


* [TP ](./15_tuples/tp/c15_tp.md)   

---


# Chapitre 16 : Représentations des nombres réels  

* [Cours : Nombres réels ](./16_reels/cours_reels.md)  [![pdf](./fig/pdf.png)](./16_reels/cours_reels.pdf)  

* [TP : Nombres réels](./16_reels/tp_reels/tp_reels.md)  

--- 

# Chapitre 17 : Représentations des caractères en machine

* [Activité : codages de caractères ](./17_caracteres/ac_intro/c17_caracteres_ac_intro.md) 


* [Cours ](./17_caracteres/cours/c17_caracteres_cours.md)  [![pdf](./fig/pdf.png)](./17_caracteres/cours/c17_caracteres_cours.pdf)  


* [Exercices ](./17_caracteres/tp_caracteres/c17_tp_caracteres.md) 


--- 

# Chapitre 18 : Les dictionnaires

* [Cours : le type construit dictionnaire en Python ](./18_dicos/c18_dicos.md)   [![pdf](./fig/pdf.png)](./18_dicos/c18_dicos.pdf)  [![Notebook Tuples](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_dicos%2Fdicos.ipynb)  

* 🎬 [Cours "La maison Lumni" sur France 4 ](https://www.lumni.fr/video/les-dictionnaires)

* [TP ](./18_dicos/tp/c18_tp_dicos.md) 

---

# Chapitre 19 : Traitement des données en table

## PARTIE 1 : Fichiers CSV et Recherche dans une table

* [Cours : les tables](./19_tables/partie1_recherche_tables/cours_p1_tables/c19_p1_csv.md)  [![pdf](./fig/pdf.png)](./19_tables/partie1_recherche_tables/cours_p1_tables/c19_p1_csv.pdf)  [![Notebook Tables](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_recherche_tables%2Ftables.ipynb)  

* [TP 1 : les tables ](./19_tables/partie1_recherche_tables/tp_p1_tables/tp1_tables.md) 

---

# Chapitre 20 : Recherche dichotomique

* [Activité introductive ](./20_dichotomie/ac_intro_dichotomie/ac_into_dichotomie.md)   [![Notebook Dichotomie](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master?filepath=nb_dichotomie%2Fdichotomie.ipynb)  

* [Cours ](./20_dichotomie/cours/cours_dichotomie.md)  [![pdf](./fig/pdf.png)](./20_dichotomie/cours/cours_dichotomie.pdf)    

* 🎬 [Recherche d'une valeur dans un tableau en vidéo ](https://www.youtube.com/watch?v=ULr_8ocz0AU&feature=youtu.be)

* [TP : quelques exemples ](./20_dichotomie/tp_dichotomie/tp_dichotomie.md) 

---


# Chapitre 21 : Algorithmes Gloutons

* [Cours : Méthode gloutonne ](./21_algos_gloutons/cours/cours_algo_glouton.md)   [![pdf](./fig/pdf.png)](./21_algos_gloutons/cours/cours_algo_glouton.pdf) 

* [TP : quelques exemples ](./21_algos_gloutons/tp/tp_algo_glouton.md) 

---


# Chapitre 22 : Algorithmes des plus proches voisins 

* [Cours ](./22_knn/cours/cours_knn.md)  [![pdf](./fig/pdf.png)](./22_knn/cours/cours_knn.pdf) 

* [TP KNN ](./22_knn/tp/tp_knn.md)  




