import matplotlib.pyplot as plt


# Exemple d'affichage de 4 graphiques
# quadrillage utilisé  : 2 lignes 2 colonnes

plt.subplot(221)
#  subplot(lcn) : l nbre de lignes, c : nbre de colonnes, n : numéro du graphique   
a = [1, 2, 3, 4, 5]
b = [1, 2, 3, 4, 5]
plt.title("Graphique n° 1")
plt.plot(a, b)
    
plt.subplot(222)
c = [10, 20, 30, 40, 50]
d = [12, 25, 38, 46, 80]
plt.title("Graphique n° 2")
plt.plot(c, d)

plt.subplot(223)
e = [0, 2, 4, 6, 8]
f = [15, 20, 80, 95, 100]
plt.title("Graphique n° 3")
plt.plot(e, f)

plt.subplot(224)
g = [1, 2, 3, 4, 5]
h = [0, 0, 4, 8, 12]
plt.title("Graphique n° 4")
plt.plot(g, h)
    
plt.tight_layout()  # permet d'éviter les problèmes de syperposition des titres
    
plt.show()
