---
title: "Ressources, conseils, liens"
lang: fr
---
# Python


## Ressources

* [Documentation officielle](https://docs.python.org/fr/3/) du langage Python en français

* [Manuel Python](http://www.inforef.be/swi/download/apprendre_python3_5.pdf) de Gerard Swinen

* [Memento Python](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf)


## THONNY

* [Tour d'horizon](./thonny/tour_thonny.md) de THONNY


## Outils

* [Repl.it](https://repl.it/languages/python3) : Editeur Python en ligne  



# HTML/CSS

* [Connaisssances de base en HTML ET CSS ](http://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/html/cours_html_css_nsi/accueil_cours_html_css_nsi.html)

* [Référence HTML sur MDN](https://developer.mozilla.org/fr/docs/Web/HTML)

* [Référence CSS sur MDN](https://developer.mozilla.org/fr/docs/Web/CSS)



# Linux

* [Quelques commandes fondamentales](https://juliend.github.io/linux-cheatsheet/)

* [Terminal Linux en ligne](http://weblinux.univ-reunion.fr/?cpu=asm&n=1) pour tester des commandes


# Pour aller plus loin

* [Le Projet EULER](https://projecteuler.net/about) propose des problèmes à résoudre à l'aide de la programmation
